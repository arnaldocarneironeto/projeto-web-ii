package model;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Administrador extends Usuario
{
    private static final long serialVersionUID = 5979558485937625904L;
    
    public Administrador()
    {
        super();
        setTipo("adminstrador");
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj instanceof Administrador ? super.equals(obj) : false;
    }

    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    @Override
    public String getTipo()
    {
        return "administrador";
    }

    @Override
    public void setTipo(String tipo)
    {
        super.setTipo(tipo);
    }
}