package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Equipamento.findAll", query = "SELECT a FROM Equipamento a"),
    @NamedQuery(name = "Equipamento.findByAtivo", query = "SELECT a FROM Equipamento a WHERE a.ativo = :ativo"),
    @NamedQuery(name = "Equipamento.findByDescricao", query = "SELECT a FROM Equipamento a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "Equipamento.findById", query = "SELECT a FROM Equipamento a WHERE a.id = :id"),
    @NamedQuery(name = "Equipamento.findByTipo", query = "SELECT a FROM Equipamento a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Equipamento.findByTombamento", query = "SELECT a FROM Equipamento a WHERE a.tombamento = :tombamento")
})
public class Equipamento implements Serializable
{
    private static final long serialVersionUID = 4952610248593104625L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 14, nullable = false, unique = true)
    private String tombamento;

    @Column(length = 140, nullable = false)
    private String descricao;

    @OneToOne
    private TipoDeEquipamento tipo;

    private boolean ativo;

    public Equipamento()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTombamento()
    {
        return tombamento;
    }

    public void setTombamento(String tombamento)
    {
        this.tombamento = tombamento;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public TipoDeEquipamento getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoDeEquipamento tipo)
    {
        this.tipo = tipo;
    }

    public boolean isAtivo()
    {
        return ativo;
    }

    public void setAtivo(boolean ativo)
    {
        this.ativo = ativo;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getTombamento() + "-" + getDescricao();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Equipamento)
        {
            Equipamento equipamento = (Equipamento) obj;
            result = (equipamento.isAtivo() == this.isAtivo()) &&
                     (equipamento.getId() == null ? this.getId() == null : equipamento.getId().equals(this.getId())) &&
                     (equipamento.getTombamento() == null ? this.getTombamento() == null : equipamento.getTombamento().equals(this.getTombamento())) &&
                     (equipamento.getDescricao() == null ? this.getDescricao() == null : equipamento.getDescricao().equals(this.getDescricao())) &&
                     (equipamento.getTipo() == null ? this.getTipo() == null : equipamento.getTipo().equals(this.getTipo()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}