package model;

public enum Estado
{
    /**
     * O chamado encontra-se aberto
     */
    ABERTO,	 

    /**
     * O chamado encontra-se fechado
     */
    FECHADO;	 
}