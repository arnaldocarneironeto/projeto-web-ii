package model;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Tecnico extends Usuario
{
    private static final long serialVersionUID = -2580839772987012661L;
    
    public Tecnico()
    {
        super();
        setTipo("tecnico");
    }

    @Override
    public boolean equals(Object obj)
    {
        return obj instanceof Tecnico ? super.equals(obj) : false;
    }

    @Override
    public int hashCode()
    {
        return super.hashCode();
    }

    @Override
    public String getTipo()
    {
        return "tecnico";
    }

    @Override
    public void setTipo(String tipo)
    {
        super.setTipo(tipo);
    }
}