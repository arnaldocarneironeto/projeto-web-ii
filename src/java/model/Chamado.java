package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Chamado.findAll" , query = "SELECT a FROM Chamado a"),
    @NamedQuery(name = "Chamado.findById", query = "SELECT a FROM Chamado a WHERE a.id = :id"),
    @NamedQuery(name = "Chamado.findByDescricaoDoProblema", query = "SELECT a FROM Chamado a WHERE a.descricaoDoProblema = :descricaoDoProblema"),
    @NamedQuery(name = "Chamado.findByEstado", query = "SELECT a FROM Chamado a WHERE a.estado = :estado"),
    @NamedQuery(name = "Chamado.findByListaDeAcoes", query = "SELECT a FROM Chamado a WHERE a.listaDeAcoes = :listaDeAcoes"),
    @NamedQuery(name = "Chamado.findByTecnicoResponsavel", query = "SELECT a FROM Chamado a WHERE a.tecnicoResponsavel = :tecnicoResponsavel"),
    @NamedQuery(name = "Chamado.findByTipo", query = "SELECT a FROM Chamado a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Chamado.findByUsuario", query = "SELECT a FROM Chamado a WHERE a.usuario = :usuario")
})
public class Chamado implements Serializable
{    
    private static final long serialVersionUID = -8338049839465073255L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @JoinColumn(nullable = false)
    @OneToOne
    private TipoDeChamado tipo;

    @JoinColumn(nullable = false)
    @OneToOne
    private Usuario usuario;

    @OneToOne
    private Tecnico tecnicoResponsavel;

    @Column(nullable = false, length = 500)
    private String descricaoDoProblema;

    private List<Acao> listaDeAcoes;

    @Enumerated(EnumType.STRING)
    private Estado estado;

    public Chamado()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public TipoDeChamado getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoDeChamado tipo)
    {
        this.tipo = tipo;
    }

    public Usuario getUsuario()
    {
        return usuario;
    }

    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

    public Tecnico getTecnicoResponsavel()
    {
        return tecnicoResponsavel;
    }

    public void setTecnicoResponsavel(Tecnico tecnicoResponsavel)
    {
        this.tecnicoResponsavel = tecnicoResponsavel;
    }

    public String getDescricaoDoProblema()
    {
        return descricaoDoProblema;
    }

    public void setDescricaoDoProblema(String descricaoDoProblema)
    {
        this.descricaoDoProblema = descricaoDoProblema;
    }

    public List<Acao> getListaDeAcoes()
    {
        return listaDeAcoes;
    }

    public void setListaDeAcoes(List<Acao> listaDeAcoes)
    {
        this.listaDeAcoes = listaDeAcoes;
    }

    public Estado getEstado()
    {
        return estado;
    }

    public void setEstado(Estado estado)
    {
        this.estado = estado;
    }
    
    @Override
    public String toString()
    {
        return getId() + "-" + getDescricaoDoProblema();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Chamado)
        {
            Chamado chamado = (Chamado) obj;
            result = (chamado.getId() == null ? this.getId() == null : chamado.getId().equals(this.getId())) &&
                     (chamado.getDescricaoDoProblema() == null ? this.getDescricaoDoProblema() == null : chamado.getDescricaoDoProblema().equals(this.getDescricaoDoProblema())) &&
                     (chamado.getEstado() == null ? this.getEstado() == null : chamado.getEstado().equals(this.getEstado())) &&
                     (chamado.getTipo() == null ? this.getTipo() == null : chamado.getTipo().equals(this.getTipo())) &&
                     (chamado.getUsuario() == null ? this.getUsuario() == null : chamado.getUsuario().equals(this.getUsuario())) &&
                     (chamado.getTecnicoResponsavel() == null ? this.getTecnicoResponsavel() == null : chamado.getTecnicoResponsavel().equals(this.getTecnicoResponsavel())) &&
                     (chamado.getListaDeAcoes() == null ? this.getListaDeAcoes() == null : chamado.getListaDeAcoes().size() == this.getListaDeAcoes().size());
            if(result == true)
            {
                result = equalLists(chamado, result);
            }
        }
        return result;
    }

    private boolean equalLists(Chamado chamado, boolean result)
    {
        List<Acao> lista1 = chamado.getListaDeAcoes();
        List<Acao> lista2 = this.getListaDeAcoes();
        for(int i = 0; i < lista1.size(); i++)
        {
            result &= lista1.get(i).equals(lista2.get(i));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    public boolean temEquipamentoAssociado()
    {
        return false;
    }
}