package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "TipoDeEquipamento.findAll", query = "SELECT a FROM TipoDeEquipamento a"),
    @NamedQuery(name = "TipoDeEquipamento.findById", query = "SELECT a FROM TipoDeEquipamento a WHERE a.id = :id"),
    @NamedQuery(name = "TipoDeEquipamento.findByNome", query = "SELECT a FROM TipoDeEquipamento a WHERE a.nome = :nome")
})
public class TipoDeEquipamento implements Serializable
{    
    private static final long serialVersionUID = -6873789294950455595L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 120, nullable = false, unique = true)
    private String nome;

    public TipoDeEquipamento()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getNome();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof TipoDeEquipamento)
        {
            TipoDeEquipamento tipo = (TipoDeEquipamento) obj;
            result = (tipo.getId() == null ? this.getId() == null : tipo.getId().equals(this.getId())) &&
                     (tipo.getNome() == null ? this.getNome() == null : tipo.getNome().equals(this.getNome()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}