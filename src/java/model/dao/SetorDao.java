package model.dao;

import java.util.List;
import model.Setor;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface SetorDao
{
    public void save(Setor setor);
    public void delete(Setor setor);
    public List<Setor> list();
    public Setor find(String sigla);
    public Setor find(Long id);
    public Setor findByNome(String nome);
}