package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Administrador;
import model.dao.AdministradorDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileAdministradorDao implements AdministradorDao
{
    private File adminstradorFile = null;
    private HashMap<Long, Administrador> mapaDeAdministradores = null;
    private final Random rnd = new SecureRandom();

    public FileAdministradorDao()
    {
        this.adminstradorFile = new File("adminstradores.dat");
        loadFromDisk();
    }

    @Override
    public void save(Administrador adminstrador)
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        if(adminstrador != null && this.mapaDeAdministradores != null)
        {
            ensureIdExists(adminstrador);
            this.mapaDeAdministradores.put(adminstrador.getId(), adminstrador);
            saveToDisk();
        }   
    }

    @Override
    public void delete(Administrador adminstrador)
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        if(adminstrador != null && this.mapaDeAdministradores != null)
        {
            if(this.mapaDeAdministradores.get(adminstrador.getId()) != null)
            {
                this.mapaDeAdministradores.remove(adminstrador.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Administrador> list()
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        ArrayList<Administrador> result = new ArrayList<>();
        if(this.mapaDeAdministradores != null)
        {
            result.addAll(this.mapaDeAdministradores.values());
        }
        return result;
    }

    @Override
    public Administrador find(String login)
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        Administrador result = null;
        for(Administrador adminstrador: list())
        {
            if(adminstrador.getLogin().equals(login))
            {
                result = adminstrador;
            }
        }
        return result;
    }

    @Override
    public Administrador find(Long id)
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeAdministradores.get(id): null;
    }

    @Override
    public Administrador findByEmail(String email)
    {
        if(this.mapaDeAdministradores == null)
        {
            loadFromDisk();
        }
        Administrador result = null;
        for(Administrador adminstrador: list())
        {
            if(adminstrador.getEmail().equals(email))
            {
                result = adminstrador;
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.adminstradorFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.adminstradorFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeAdministradores);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileAdministradorDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.adminstradorFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.adminstradorFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeAdministradores = (HashMap<Long, Administrador>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeAdministradores = new HashMap<>();
            }
        }
    }

    private void ensureIdExists(Administrador adminstrador)
    {
        if(adminstrador.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeAdministradores.containsKey(id));
            adminstrador.setId(id);
        }
    }
}