package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Administrador;
import model.Setor;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.SetorDao;
import model.dao.UsuarioDao;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileUsuarioDao implements UsuarioDao
{
    private File usuarioFile = null;
    private HashMap<Long, Usuario> mapaDeUsuarios = null;
    private final Random rnd = new SecureRandom();

    public FileUsuarioDao()
    {
        this.usuarioFile = new File("usuarios.dat");
        if(this.usuarioFile.exists() == false)
        {
            criarUsuarioAdminstradorComSenhaPadrao();
        }
        loadFromDisk();
    }

    @Override
    public void save(Usuario usuario)
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        if(usuario != null && this.mapaDeUsuarios != null)
        {
            ensureIdExists(usuario);
            this.mapaDeUsuarios.put(usuario.getId(), usuario);
            saveToDisk();
        }   
    }

    @Override
    public void delete(Usuario usuario)
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        if(usuario != null && this.mapaDeUsuarios != null)
        {
            if(this.mapaDeUsuarios.get(usuario.getId()) != null)
            {
                this.mapaDeUsuarios.remove(usuario.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Usuario> list()
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        ArrayList<Usuario> result = new ArrayList<>();
        if(this.mapaDeUsuarios != null)
        {
            result.addAll(this.mapaDeUsuarios.values());
        }
        return result;
    }

    @Override
    public Usuario find(String login)
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        Usuario result = null;
        for(Usuario usuario: list())
        {
            if(usuario.getLogin().equals(login))
            {
                result = usuario;
            }
        }
        return result;
    }

    @Override
    public Usuario find(Long id)
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeUsuarios.get(id): null;
    }

    @Override
    public Usuario findByEmail(String email)
    {
        if(this.mapaDeUsuarios == null)
        {
            loadFromDisk();
        }
        Usuario result = null;
        for(Usuario usuario: list())
        {
            if(usuario.getEmail().equals(email))
            {
                result = usuario;
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.usuarioFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.usuarioFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeUsuarios);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileUsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.usuarioFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.usuarioFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeUsuarios = (HashMap<Long, Usuario>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeUsuarios = new HashMap<>();
            }
        }
    }
    
    private Setor criarSetor(String nome, String sigla)
    {
        DaoFactory df = new FileFactory();
        SetorDao sd = df.createSetorDao();
        
        Setor setor = new Setor();
        setor.setId(00L);
        setor.setNome(nome);
        setor.setSigla(sigla);
        
        sd.save(setor);
        return setor;
    }

    private void criarUsuarioAdminstradorComSenhaPadrao()
    {
        Usuario admin = new Administrador();
        admin.setId(00l);
        admin.setLogin("admin");
        admin.setSenha("admin");
        admin.setEmail("arnymal@gmail.com");
        admin.setAtivo(true);
        admin.setDataDeCriacaoDaSenha(Calendar.getInstance());
        admin.setFoiAprovado(true);
        admin.setNome("Arnaldo Carneiro");
        admin.setSala("Sem sala");
        admin.setSalt(IdGenerator.generateId());
        Setor setor = criarSetor("Desenvolvimento", "DEV");
        admin.setSetor(setor);
        admin.setTelefone("");
        mapaDeUsuarios = new HashMap<>();
        mapaDeUsuarios.put(admin.getId(), admin);
        saveToDisk();
    }

    private void ensureIdExists(Usuario usuario)
    {
        if(usuario.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeUsuarios.containsKey(id));
            usuario.setId(id);
        }
    }
}