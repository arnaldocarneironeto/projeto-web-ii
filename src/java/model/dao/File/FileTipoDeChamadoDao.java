package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TipoDeChamado;
import model.dao.TipoDeChamadoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileTipoDeChamadoDao implements TipoDeChamadoDao
{
    private File tipoDeChamadoFile = null;
    private HashMap<Long, TipoDeChamado> mapaDeTiposDeChamado = null;
    private final Random rnd = new SecureRandom();

    public FileTipoDeChamadoDao()
    {
        this.tipoDeChamadoFile = new File("tiposDeChamado.dat");
        loadFromDisk();
    }

    @Override
    public void save(TipoDeChamado tipoDeChamado)
    {
        if(this.mapaDeTiposDeChamado == null)
        {
            loadFromDisk();
        }
        if(tipoDeChamado != null && isValidString(tipoDeChamado.getNome()) && this.mapaDeTiposDeChamado != null)
        {
            ensureIdExists(tipoDeChamado);
            this.mapaDeTiposDeChamado.put(tipoDeChamado.getId(), tipoDeChamado);
            saveToDisk();
        }   
    }

    @Override
    public void delete(TipoDeChamado tipoDeChamado)
    {
        if(this.mapaDeTiposDeChamado == null)
        {
            loadFromDisk();
        }
        if(tipoDeChamado != null && this.mapaDeTiposDeChamado != null)
        {
            if(this.mapaDeTiposDeChamado.get(tipoDeChamado.getId()) != null)
            {
                this.mapaDeTiposDeChamado.remove(tipoDeChamado.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<TipoDeChamado> list()
    {
        if(this.mapaDeTiposDeChamado == null)
        {
            loadFromDisk();
        }
        ArrayList<TipoDeChamado> result = new ArrayList<>();
        if(this.mapaDeTiposDeChamado != null)
        {
            result.addAll(this.mapaDeTiposDeChamado.values());
        }
        return result;
    }

    @Override
    public TipoDeChamado find(String nome)
    {
        if(this.mapaDeTiposDeChamado == null)
        {
            loadFromDisk();
        }
        TipoDeChamado result = null;
        for(TipoDeChamado tipoDeChamado: list())
        {
            if(tipoDeChamado.getNome().equals(nome))
            {
                result = tipoDeChamado;
            }
        }
        return result;
    }

    @Override
    public TipoDeChamado find(Long id)
    {
        if(this.mapaDeTiposDeChamado == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeTiposDeChamado.get(id): null;
    }

    private void saveToDisk()
    {
        if(this.tipoDeChamadoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.tipoDeChamadoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeTiposDeChamado);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileTipoDeChamadoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.tipoDeChamadoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.tipoDeChamadoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeTiposDeChamado = (HashMap<Long, TipoDeChamado>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeTiposDeChamado = new HashMap<>();
            }
        }
    }

    private boolean isValidString(String string)
    {
        return string != null && string.isEmpty() == false;
    }

    private void ensureIdExists(TipoDeChamado tipoDeChamado)
    {
        if(tipoDeChamado.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeTiposDeChamado.containsKey(id));
            tipoDeChamado.setId(id);
        }
    }
}