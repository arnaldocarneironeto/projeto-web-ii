package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Chamado;
import model.ChamadoDeEquipamento;
import model.Equipamento;
import model.Estado;
import model.Tecnico;
import model.Usuario;
import model.dao.ChamadoDeEquipamentoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileChamadoDeEquipamentoDao implements ChamadoDeEquipamentoDao
{
    private File chamadoFile = null;
    private HashMap<Long, Chamado> mapaDeChamados = null;
    private final Random rnd = new SecureRandom();

    public FileChamadoDeEquipamentoDao()
    {
        this.chamadoFile = new File("chamados.dat");
        loadFromDisk();
    }

    @Override
    public void save(ChamadoDeEquipamento chamadoDeEquipamento)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        if(chamadoDeEquipamento != null && chamadoDeEquipamento.getUsuario() != null &&
           chamadoDeEquipamento.getEquipamento() != null && this.mapaDeChamados != null)
        {
            ensureIdExists(chamadoDeEquipamento);
            this.mapaDeChamados.put(chamadoDeEquipamento.getId(), chamadoDeEquipamento);
            saveToDisk();
        }
    }

    @Override
    public void delete(ChamadoDeEquipamento chamadoDeEquipamento)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        if(chamadoDeEquipamento != null && this.mapaDeChamados != null)
        {
            if(this.mapaDeChamados.get(chamadoDeEquipamento.getId()) != null)
            {
                this.mapaDeChamados.remove(chamadoDeEquipamento.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<ChamadoDeEquipamento> list()
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        ArrayList<ChamadoDeEquipamento> result = new ArrayList<>();
        if(this.mapaDeChamados != null)
        {
            for(Chamado chamado: this.mapaDeChamados.values())
            {
                if(chamado.temEquipamentoAssociado())
                {
                    result.add((ChamadoDeEquipamento) chamado);
                }
            }
        }
        return result;
    }

    @Override
    public ChamadoDeEquipamento find(Long id)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        Chamado possibleResult = null;
        if(id != null)
        {
            possibleResult = this.mapaDeChamados.get(id);
            if(possibleResult.temEquipamentoAssociado() == false)
            {
                possibleResult = null;                
            }
        }
        return possibleResult != null ? (ChamadoDeEquipamento) possibleResult : null;
    }
    
    @Override
    public List<ChamadoDeEquipamento> findByEstado(Estado estado)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        for(ChamadoDeEquipamento chamado : list())
        {
            if(chamado.getEstado().equals(estado))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByTecnicoResponsavel(Long tecnicoId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        for(ChamadoDeEquipamento chamado : list())
        {
            if(chamado.getTecnicoResponsavel() != null && chamado.getTecnicoResponsavel().getId().equals(tecnicoId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByTecnicoResponsavel(Tecnico tecnico)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        if(tecnico != null)
        {
            result.addAll(findByTecnicoResponsavel(tecnico.getId()));
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByUsuario(Long usuarioId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        for(ChamadoDeEquipamento chamado : list())
        {
            if(chamado.getUsuario().getId().equals(usuarioId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByUsuario(Usuario usuario)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        if(usuario != null)
        {
            result.addAll(findByUsuario(usuario.getId()));
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByTipo(Long tipoDeChamadoId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        for(ChamadoDeEquipamento chamado : list())
        {
            if(chamado.getTipo().getId().equals(tipoDeChamadoId))
            {
                result.add(chamado);
            }
        }
        return result;
    }
    
    @Override
    public List<ChamadoDeEquipamento> findByEquipamento(Equipamento equipamento)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        if(equipamento != null)
        {
            result.addAll(findByEquipamento(equipamento.getId()));
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByEquipamento(Long equipamentoId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        for(ChamadoDeEquipamento chamado : list())
        {
            if(chamado.getEquipamento().getId().equals(equipamentoId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<ChamadoDeEquipamento> findByEquipamento(String tombamento)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<ChamadoDeEquipamento> result = new ArrayList<>();
        if(isValidString(tombamento))
        {
            for(ChamadoDeEquipamento chamado : list())
            {
                if(chamado.getEquipamento().getTombamento().equals(tombamento))
                {
                    result.add(chamado);
                }
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.chamadoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.chamadoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeChamados);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileChamadoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.chamadoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.chamadoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeChamados = (HashMap<Long, Chamado>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeChamados = new HashMap<>();
            }
        }
    }

    private boolean isValidString(String tombamento)
    {
        return tombamento != null && tombamento.isEmpty() == false;
    }

    private void ensureIdExists(ChamadoDeEquipamento chamadoDeEquipamento)
    {
        if(chamadoDeEquipamento.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeChamados.containsKey(id));
            chamadoDeEquipamento.setId(id);
        }
    }
}