package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Token;
import model.dao.TokenDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileTokenDao implements TokenDao
{
    private File tokenFile = null;
    private HashMap<Long, Token> mapaDeTokens = null;
    private final Random rnd = new SecureRandom();

    public FileTokenDao()
    {
        this.tokenFile = new File("tokens.dat");
        loadFromDisk();
    }

    @Override
    public void save(Token token)
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        if(token != null && token.getUsuarioAssociado() != null && this.mapaDeTokens != null)
        {
            ensureIdExists(token);
            this.mapaDeTokens.put(token.getId(), token);
            saveToDisk();
        }   
    }

    @Override
    public void delete(Token token)
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        if(token != null && this.mapaDeTokens != null)
        {
            if(this.mapaDeTokens.get(token.getId()) != null)
            {
                this.mapaDeTokens.remove(token.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Token> list()
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        ArrayList<Token> result = new ArrayList<>();
        if(this.mapaDeTokens != null)
        {
            result.addAll(this.mapaDeTokens.values());
        }
        return result;
    }

    @Override
    public Token find(String codigo)
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        Token result = null;
        for(Token token: list())
        {
            if(token.getCodigo().equals(codigo))
            {
                result = token;
            }
        }
        return result;
    }

    @Override
    public Token find(Long id)
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeTokens.get(id): null;
    }

    @Override
    public List<Token> findByUserId(Long userId)
    {
        if(this.mapaDeTokens == null)
        {
            loadFromDisk();
        }
        List<Token> result = new ArrayList<>();
        if(userId != null)
        {
            for(Token token: list())
            {
                if(userId.equals(token.getUsuarioAssociado().getId()))
                {
                    result.add(token);
                }
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.tokenFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.tokenFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeTokens);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileTokenDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.tokenFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.tokenFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeTokens = (HashMap<Long, Token>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeTokens = new HashMap<>();
            }
        }
    }

    private void ensureIdExists(Token token)
    {
        if(token.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeTokens.containsKey(id));
            token.setId(id);
        }
    }
}