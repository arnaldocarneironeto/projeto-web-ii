package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Equipamento;
import model.dao.EquipamentoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileEquipamentoDao implements EquipamentoDao
{
    private File equipamentoFile = null;
    private HashMap<Long, Equipamento> mapaDeEquipamentos = null;
    private final Random rnd = new SecureRandom();

    public FileEquipamentoDao()
    {
        this.equipamentoFile = new File("equipamentos.dat");
        loadFromDisk();
    }

    @Override
    public void save(Equipamento equipamento)
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        if(equipamento != null && isValidString(equipamento.getTombamento()) && this.mapaDeEquipamentos != null)
        {
            ensureIdExists(equipamento);
            this.mapaDeEquipamentos.put(equipamento.getId(), equipamento);
            saveToDisk();
        }
    }

    @Override
    public void delete(Equipamento equipamento)
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        if(equipamento != null && this.mapaDeEquipamentos != null)
        {
            if(this.mapaDeEquipamentos.get(equipamento.getId()) != null)
            {
                this.mapaDeEquipamentos.remove(equipamento.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Equipamento> list()
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        ArrayList<Equipamento> result = new ArrayList<>();
        if(this.mapaDeEquipamentos != null)
        {
            result.addAll(this.mapaDeEquipamentos.values());
        }
        return result;
    }

    @Override
    public Equipamento find(String tombamento)
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        Equipamento result = null;
        for(Equipamento equipamento : list())
        {
            if(equipamento.getTombamento().equals(tombamento))
            {
                result = equipamento;
            }
        }
        return result;
    }

    @Override
    public Equipamento find(Long id)
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        return id != null ? this.mapaDeEquipamentos.get(id) : null;
    }

    @Override
    public List<Equipamento> findByTipoId(Long tipoId)
    {
        if(this.mapaDeEquipamentos == null)
        {
            loadFromDisk();
        }
        List<Equipamento> result = new ArrayList<>();
        for(Equipamento equipamento : list())
        {
            if(equipamento.getTipo().getId().equals(tipoId))
            {
                result.add(equipamento);
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.equipamentoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.equipamentoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeEquipamentos);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileEquipamentoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.equipamentoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.equipamentoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeEquipamentos = (HashMap<Long, Equipamento>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeEquipamentos = new HashMap<>();
            }
        }
    }

    private boolean isValidString(String string)
    {
        return string != null && string.isEmpty() == false;
    }

    private void ensureIdExists(Equipamento equipamento)
    {
        if(equipamento.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeEquipamentos.containsKey(id));
            equipamento.setId(id);
        }
    }
}