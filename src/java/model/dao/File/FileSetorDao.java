package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setor;
import model.dao.SetorDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileSetorDao implements SetorDao
{
    private File setorFile = null;
    private HashMap<Long, Setor> mapaDeSetores = null;
    private final Random rnd = new SecureRandom();

    public FileSetorDao()
    {
        this.setorFile = new File("setores.dat");
        loadFromDisk();
    }

    @Override
    public void save(Setor setor)
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        if(setor != null && isValidString(setor.getNome()) && this.mapaDeSetores != null)
        {
            ensureIdExists(setor);
            this.mapaDeSetores.put(setor.getId(), setor);
            saveToDisk();
        }   
    }

    @Override
    public void delete(Setor setor)
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        if(setor != null && this.mapaDeSetores != null)
        {
            if(this.mapaDeSetores.get(setor.getId()) != null)
            {
                this.mapaDeSetores.remove(setor.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Setor> list()
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        ArrayList<Setor> result = new ArrayList<>();
        if(this.mapaDeSetores != null)
        {
            result.addAll(this.mapaDeSetores.values());
        }
        return result;
    }

    @Override
    public Setor find(String sigla)
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        Setor result = null;
        for(Setor setor: list())
        {
            if(setor.getNome().equals(sigla))
            {
                result = setor;
            }
        }
        return result;
    }

    @Override
    public Setor find(Long id)
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeSetores.get(id): null;
    }

    @Override
    public Setor findByNome(String nome)
    {
        if(this.mapaDeSetores == null)
        {
            loadFromDisk();
        }
        Setor result = null;
        for(Setor setor: list())
        {
            if(setor.getNome().equals(nome))
            {
                result = setor;
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.setorFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.setorFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeSetores);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileSetorDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.setorFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.setorFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeSetores = (HashMap<Long, Setor>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeSetores = new HashMap<>();
            }
        }
    }

    private boolean isValidString(String string)
    {
        return string != null && string.isEmpty() == false;
    }

    private void ensureIdExists(Setor setor)
    {
        if(setor.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeSetores.containsKey(id));
            setor.setId(id);
        }
    }
}