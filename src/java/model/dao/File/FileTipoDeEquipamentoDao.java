package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.TipoDeEquipamento;
import model.dao.TipoDeEquipamentoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileTipoDeEquipamentoDao implements TipoDeEquipamentoDao
{    
    private File tipoDeEquipamentoFile = null;
    private HashMap<Long, TipoDeEquipamento> mapaDeTiposDeEquipamento = null;
    private final Random rnd = new SecureRandom();

    public FileTipoDeEquipamentoDao()
    {
        this.tipoDeEquipamentoFile = new File("tiposDeEquipamento.dat");
        loadFromDisk();
    }

    @Override
    public void save(TipoDeEquipamento tipoDeEquipamento)
    {
        if(this.mapaDeTiposDeEquipamento == null)
        {
            loadFromDisk();
        }
        if(tipoDeEquipamento != null && isValidString(tipoDeEquipamento.getNome()) && this.mapaDeTiposDeEquipamento != null)
        {
            ensureIdExists(tipoDeEquipamento);
            this.mapaDeTiposDeEquipamento.put(tipoDeEquipamento.getId(), tipoDeEquipamento);
            saveToDisk();
        }   
    }

    @Override
    public void delete(TipoDeEquipamento tipoDeEquipamento)
    {
        if(this.mapaDeTiposDeEquipamento == null)
        {
            loadFromDisk();
        }
        if(tipoDeEquipamento != null && this.mapaDeTiposDeEquipamento != null)
        {
            if(this.mapaDeTiposDeEquipamento.get(tipoDeEquipamento.getId()) != null)
            {
                this.mapaDeTiposDeEquipamento.remove(tipoDeEquipamento.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<TipoDeEquipamento> list()
    {
        if(this.mapaDeTiposDeEquipamento == null)
        {
            loadFromDisk();
        }
        ArrayList<TipoDeEquipamento> result = new ArrayList<>();
        if(this.mapaDeTiposDeEquipamento != null)
        {
            result.addAll(this.mapaDeTiposDeEquipamento.values());
        }
        return result;
    }

    @Override
    public TipoDeEquipamento find(String nome)
    {
        if(this.mapaDeTiposDeEquipamento == null)
        {
            loadFromDisk();
        }
        TipoDeEquipamento result = null;
        for(TipoDeEquipamento tipoDeEquipamento: list())
        {
            if(tipoDeEquipamento.getNome().equals(nome))
            {
                result = tipoDeEquipamento;
            }
        }
        return result;
    }

    @Override
    public TipoDeEquipamento find(Long id)
    {
        if(this.mapaDeTiposDeEquipamento == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeTiposDeEquipamento.get(id): null;
    }

    private void saveToDisk()
    {
        if(this.tipoDeEquipamentoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.tipoDeEquipamentoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeTiposDeEquipamento);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileTipoDeEquipamentoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.tipoDeEquipamentoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.tipoDeEquipamentoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeTiposDeEquipamento = (HashMap<Long, TipoDeEquipamento>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeTiposDeEquipamento = new HashMap<>();
            }
        }
    }

    private boolean isValidString(String string)
    {
        return string != null && string.isEmpty() == false;
    }

    private void ensureIdExists(TipoDeEquipamento tipoDeEquipamento)
    {
         if(tipoDeEquipamento.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeTiposDeEquipamento.containsKey(id));
            tipoDeEquipamento.setId(id);
        }
    }
}