package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Chamado;
import model.Estado;
import model.Tecnico;
import model.Usuario;
import model.dao.ChamadoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileChamadoDao implements ChamadoDao
{
    private File chamadoFile = null;
    private HashMap<Long, Chamado> mapaDeChamados = null;
    private final Random rnd = new SecureRandom();

    public FileChamadoDao()
    {
        this.chamadoFile = new File("chamados.dat");
        loadFromDisk();
    }

    @Override
    public void save(Chamado chamado)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        if(chamado != null && chamado.getUsuario() != null && this.mapaDeChamados != null)
        {
            ensureIdExists(chamado);
            this.mapaDeChamados.put(chamado.getId(), chamado);
            saveToDisk();
        }
    }

    @Override
    public void delete(Chamado chamado)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        if(chamado != null && this.mapaDeChamados != null)
        {
            if(this.mapaDeChamados.get(chamado.getId()) != null)
            {
                this.mapaDeChamados.remove(chamado.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Chamado> list()
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        ArrayList<Chamado> result = new ArrayList<>();
        if(this.mapaDeChamados != null)
        {
            result.addAll(this.mapaDeChamados.values());
        }
        return result;
    }

    @Override
    public Chamado find(Long id)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        return id != null ? this.mapaDeChamados.get(id) : null;
    }
    
    @Override
    public List<Chamado> findByEstado(Estado estado)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado : list())
        {
            if(chamado.getEstado().equals(estado))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<Chamado> findByTecnicoResponsavel(Long tecnicoId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado : list())
        {
            if(chamado.getTecnicoResponsavel() != null && chamado.getTecnicoResponsavel().getId().equals(tecnicoId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<Chamado> findByTecnicoResponsavel(Tecnico tecnico)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        if(tecnico != null)
        {
            result.addAll(findByTecnicoResponsavel(tecnico.getId()));
        }
        return result;
    }

    @Override
    public List<Chamado> findByUsuario(Long usuarioId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado : list())
        {
            if(chamado.getUsuario().getId().equals(usuarioId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    @Override
    public List<Chamado> findByUsuario(Usuario usuario)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        if(usuario != null)
        {
            result.addAll(findByUsuario(usuario.getId()));
        }
        return result;
    }

    @Override
    public List<Chamado> findByTipo(Long tipoDeChamadoId)
    {
        if(this.mapaDeChamados == null)
        {
            loadFromDisk();
        }
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado : list())
        {
            if(chamado.getTipo().getId().equals(tipoDeChamadoId))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.chamadoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.chamadoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeChamados);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileChamadoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.chamadoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.chamadoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeChamados = (HashMap<Long, Chamado>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeChamados = new HashMap<>();
            }
        }
    }

    private void ensureIdExists(Chamado chamado)
    {
        if(chamado.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeChamados.containsKey(id));
            chamado.setId(id);
        }
    }
}