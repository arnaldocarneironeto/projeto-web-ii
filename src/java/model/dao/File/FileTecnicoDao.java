package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Tecnico;
import model.dao.TecnicoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileTecnicoDao implements TecnicoDao
{
    private File tecnicoFile = null;
    private HashMap<Long, Tecnico> mapaDeTecnicos = null;
    private final Random rnd = new SecureRandom();

    public FileTecnicoDao()
    {
        this.tecnicoFile = new File("tecnicos.dat");
        loadFromDisk();
    }

    @Override
    public void save(Tecnico tecnico)
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        if(tecnico != null && this.mapaDeTecnicos != null)
        {
            ensureIdExists(tecnico);
            this.mapaDeTecnicos.put(tecnico.getId(), tecnico);
            saveToDisk();
        }   
    }

    @Override
    public void delete(Tecnico tecnico)
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        if(tecnico != null && this.mapaDeTecnicos != null)
        {
            if(this.mapaDeTecnicos.get(tecnico.getId()) != null)
            {
                this.mapaDeTecnicos.remove(tecnico.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Tecnico> list()
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        ArrayList<Tecnico> result = new ArrayList<>();
        if(this.mapaDeTecnicos != null)
        {
            result.addAll(this.mapaDeTecnicos.values());
        }
        return result;
    }

    @Override
    public Tecnico find(String login)
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        Tecnico result = null;
        for(Tecnico tecnico: list())
        {
            if(tecnico.getLogin().equals(login))
            {
                result = tecnico;
            }
        }
        return result;
    }

    @Override
    public Tecnico find(Long id)
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeTecnicos.get(id): null;
    }

    @Override
    public Tecnico findByEmail(String email)
    {
        if(this.mapaDeTecnicos == null)
        {
            loadFromDisk();
        }
        Tecnico result = null;
        for(Tecnico tecnico: list())
        {
            if(tecnico.getEmail().equals(email))
            {
                result = tecnico;
            }
        }
        return result;
    }

    private void saveToDisk()
    {
        if(this.tecnicoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.tecnicoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeTecnicos);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileTecnicoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.tecnicoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.tecnicoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeTecnicos = (HashMap<Long, Tecnico>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeTecnicos = new HashMap<>();
            }
        }
    }

    private void ensureIdExists(Tecnico tecnico)
    {
        if(tecnico.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeTecnicos.containsKey(id));
            tecnico.setId(id);
        }
    }
}