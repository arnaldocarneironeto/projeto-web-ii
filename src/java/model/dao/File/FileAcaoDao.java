package model.dao.File;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Acao;
import model.Tecnico;
import model.dao.AcaoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileAcaoDao implements AcaoDao
{
    private File acaoFile = null;
    private HashMap<Long, Acao> mapaDeAcoes = null;
    private final Random rnd = new SecureRandom();

    public FileAcaoDao()
    {
        this.acaoFile = new File("acoes.dat");
        loadFromDisk();
    }
    
    @Override
    public void save(Acao acao)
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        if(acao != null && this.mapaDeAcoes != null)
        {
            ensureIdExists(acao);
            this.mapaDeAcoes.put(acao.getId(), acao);
            saveToDisk();
        }
    }

    @Override
    public void delete(Acao acao)
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        if(acao != null && this.mapaDeAcoes != null)
        {
            if(this.mapaDeAcoes.get(acao.getId()) != null)
            {
                this.mapaDeAcoes.remove(acao.getId());
                saveToDisk();
            }
        }
    }

    @Override
    public List<Acao> list()
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        ArrayList<Acao> result = new ArrayList<>();
        if(this.mapaDeAcoes != null)
        {
            result.addAll(this.mapaDeAcoes.values());
        }
        return result;
    }

    @Override
    public List<Acao> findByTecnico(Tecnico tecnico)
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        return tecnico != null? findByTecnico(tecnico.getId()): new ArrayList<>();
    }

    @Override
    public List<Acao> findByTecnico(Long tecnicoId)
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        List<Acao> result = new ArrayList<>();
        for(Acao acao: list())
        {
            if(acao.getTecnico().getId().equals(tecnicoId))
            {
                result.add(acao);
            }
        }
        return result;
    }

    @Override
    public Acao find(Long id)
    {
        if(this.mapaDeAcoes == null)
        {
            loadFromDisk();
        }
        return id != null? this.mapaDeAcoes.get(id): null;
    }

    private void saveToDisk()
    {
        if(this.acaoFile != null)
        {
            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(this.acaoFile);
                ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
                outputStream.writeObject(this.mapaDeAcoes);
            }
            catch(IOException ex)
            {
                Logger.getLogger(FileAcaoDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadFromDisk()
    {
        if(this.acaoFile != null)
        {
            try
            {
                FileInputStream fileInputStream = new FileInputStream(this.acaoFile);
                ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
                this.mapaDeAcoes = (HashMap<Long, Acao>) inputStream.readObject();
            }
            catch(IOException | ClassNotFoundException ex)
            {
                this.mapaDeAcoes = new HashMap<>();
            }
        }
    }

    private void ensureIdExists(Acao acao)
    {
        if(acao.getId() == null)
        {
            Long id;
            do
            {
                id = rnd.nextLong();
            }
            while(mapaDeAcoes.containsKey(id));
            acao.setId(id);
        }
    }
}