package model.dao.File;

import model.dao.AcaoDao;
import model.dao.AdministradorDao;
import model.dao.ChamadoDao;
import model.dao.ChamadoDeEquipamentoDao;
import model.dao.DaoFactory;
import model.dao.EquipamentoDao;
import model.dao.SetorDao;
import model.dao.TecnicoDao;
import model.dao.TipoDeChamadoDao;
import model.dao.TipoDeEquipamentoDao;
import model.dao.TokenDao;
import model.dao.UsuarioDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FileFactory extends DaoFactory
{
    private static final AcaoDao acaoDao;
    private static final AdministradorDao administradorDao;
    private static final ChamadoDao chamadoDao;
    private static final ChamadoDeEquipamentoDao chamadoDeEquipamentoDao;
    private static final EquipamentoDao equipamentoDao;
    private static final SetorDao setorDao;
    private static final TecnicoDao tecnicoDao;
    private static final TipoDeChamadoDao tipoDeChamadoDao;
    private static final TipoDeEquipamentoDao tipoDeEquipamentoDao;
    private static final TokenDao tokenDao;
    private static final UsuarioDao usuarioDao;
    
    static
    {
        acaoDao = new FileAcaoDao();
        administradorDao = new FileAdministradorDao();
        chamadoDao = new FileChamadoDao();
        chamadoDeEquipamentoDao = new FileChamadoDeEquipamentoDao();
        equipamentoDao = new FileEquipamentoDao();
        setorDao = new FileSetorDao();
        tecnicoDao = new FileTecnicoDao();
        tipoDeChamadoDao = new FileTipoDeChamadoDao();
        tipoDeEquipamentoDao = new FileTipoDeEquipamentoDao();
        tokenDao = new FileTokenDao();
        usuarioDao = new FileUsuarioDao();
    }
    
    @Override
    public AcaoDao createAcaoDao()
    {
        return acaoDao;
    }

    @Override
    public AdministradorDao createAdministradorDao()
    {
        return administradorDao;
    }

    @Override
    public ChamadoDao createChamadoDao()
    {
        return chamadoDao;
    }

    @Override
    public ChamadoDeEquipamentoDao createChamadoDeEquipamentoDao()
    {
        return chamadoDeEquipamentoDao;
    }

    @Override
    public EquipamentoDao createEquipamentoDao()
    {
        return equipamentoDao;
    }

    @Override
    public SetorDao createSetorDao()
    {
        return setorDao;
    }

    @Override
    public TecnicoDao createTecnicoDao()
    {
        return tecnicoDao;
    }

    @Override
    public TipoDeChamadoDao createTipoDeChamadoDao()
    {
        return tipoDeChamadoDao;
    }

    @Override
    public TipoDeEquipamentoDao createTipoDeEquipamentoDao()
    {
        return tipoDeEquipamentoDao;
    }

    @Override
    public TokenDao createTokenDao()
    {
        return tokenDao;
    }

    @Override
    public UsuarioDao createUsuarioDao()
    {
        return usuarioDao;
    }
}