package model.dao;

import java.util.List;
import model.Token;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface TokenDao
{
    public void save(Token token);
    public void delete(Token token);
    public List<Token> list();
    public Token find(Long id);
    public Token find(String codigo);
    public List<Token> findByUserId(Long userId);
}