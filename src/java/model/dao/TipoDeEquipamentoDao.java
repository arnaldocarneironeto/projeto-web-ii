package model.dao;

import java.util.List;
import model.TipoDeEquipamento;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface TipoDeEquipamentoDao
{
    public void save(TipoDeEquipamento tipoDeEquipamento);
    public void delete(TipoDeEquipamento tipoDeEquipamento);
    public List<TipoDeEquipamento> list();
    public TipoDeEquipamento find(String nome);
    public TipoDeEquipamento find(Long id);
}