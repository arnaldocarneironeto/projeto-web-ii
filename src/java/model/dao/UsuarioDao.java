package model.dao;

import java.util.List;
import model.Usuario;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface UsuarioDao
{
    public void save(Usuario usuario);
    public void delete(Usuario usuario);
    public List<Usuario> list();
    public Usuario find(String login);
    public Usuario find(Long id);
    public Usuario findByEmail(String email);
}