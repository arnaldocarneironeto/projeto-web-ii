package model.dao;

import java.util.List;
import model.Administrador;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface AdministradorDao
{
    public void save(Administrador administrador);
    public void delete(Administrador administrador);
    public List<Administrador> list();
    public Administrador find(String login);
    public Administrador find(Long id);
    public Administrador findByEmail(String email);
}