package model.dao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public abstract class DaoFactory
{
    public abstract AcaoDao createAcaoDao();
    public abstract AdministradorDao createAdministradorDao();
    public abstract ChamadoDao createChamadoDao();
    public abstract ChamadoDeEquipamentoDao createChamadoDeEquipamentoDao();
    public abstract EquipamentoDao createEquipamentoDao();
    public abstract SetorDao createSetorDao();
    public abstract TecnicoDao createTecnicoDao();
    public abstract TipoDeChamadoDao createTipoDeChamadoDao();
    public abstract TipoDeEquipamentoDao createTipoDeEquipamentoDao();
    public abstract TokenDao createTokenDao();
    public abstract UsuarioDao createUsuarioDao();
}