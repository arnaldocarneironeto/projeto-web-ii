package model.dao;

import java.util.List;
import model.Acao;
import model.Tecnico;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface AcaoDao
{
    public void save(Acao acao);
    public void delete(Acao acao);
    public List<Acao> list();
    public List<Acao> findByTecnico(Tecnico tecnico);
    public List<Acao> findByTecnico(Long tecnicoId);
    public Acao find(Long id);
}