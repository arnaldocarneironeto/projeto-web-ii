package model.dao;

import java.util.List;
import model.ChamadoDeEquipamento;
import model.Equipamento;
import model.Estado;
import model.Tecnico;
import model.Usuario;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface ChamadoDeEquipamentoDao
{
    public void save(ChamadoDeEquipamento chamado);
    public void delete(ChamadoDeEquipamento chamado);
    public List<ChamadoDeEquipamento> list();
    public ChamadoDeEquipamento find(Long id);
    public List<ChamadoDeEquipamento> findByEstado(Estado estado);
    public List<ChamadoDeEquipamento> findByTecnicoResponsavel(Long tecnicoId);
    public List<ChamadoDeEquipamento> findByTecnicoResponsavel(Tecnico tecnico);
    public List<ChamadoDeEquipamento> findByUsuario(Long usuarioId);
    public List<ChamadoDeEquipamento> findByUsuario(Usuario usuario);
    public List<ChamadoDeEquipamento> findByTipo(Long tipoDeChamadoId);
    public List<ChamadoDeEquipamento> findByEquipamento(Equipamento equipamento);
    public List<ChamadoDeEquipamento> findByEquipamento(Long equipamentoId);
    public List<ChamadoDeEquipamento> findByEquipamento(String tombamento);
}