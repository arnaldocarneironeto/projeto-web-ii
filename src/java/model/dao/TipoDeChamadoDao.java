package model.dao;

import java.util.List;
import model.TipoDeChamado;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface TipoDeChamadoDao
{
    public void save(TipoDeChamado tipoDeChamado);
    public void delete(TipoDeChamado tipoDeChamado);
    public List<TipoDeChamado> list();
    public TipoDeChamado find(String nome);
    public TipoDeChamado find(Long id);
}