package model.dao;

import java.util.List;
import model.Chamado;
import model.Estado;
import model.Tecnico;
import model.Usuario;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface ChamadoDao
{
    public void save(Chamado chamado);
    public void delete(Chamado chamado);
    public List<Chamado> list();
    public Chamado find(Long id);
    public List<Chamado> findByEstado(Estado estado);
    public List<Chamado> findByTecnicoResponsavel(Long tecnicoId);
    public List<Chamado> findByTecnicoResponsavel(Tecnico tecnico);
    public List<Chamado> findByUsuario(Long usuarioId);
    public List<Chamado> findByUsuario(Usuario usuario);
    public List<Chamado> findByTipo(Long tipoDeChamadoId);
}