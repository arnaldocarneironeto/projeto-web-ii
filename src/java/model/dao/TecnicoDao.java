package model.dao;

import java.util.List;
import model.Tecnico;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface TecnicoDao
{
    public void save(Tecnico tecnico);
    public void delete(Tecnico tecnico);
    public List<Tecnico> list();
    public Tecnico find(String login);
    public Tecnico find(Long id);
    public Tecnico findByEmail(String email);
}