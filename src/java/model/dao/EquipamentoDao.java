package model.dao;

import java.util.List;
import model.Equipamento;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public interface EquipamentoDao
{
    public void save(Equipamento equipamento);
    public void delete(Equipamento equipamento);
    public List<Equipamento> list();
    public List<Equipamento> findByTipoId(Long tipoId);
    public Equipamento find(Long id);
    public Equipamento find(String tombamento);
}