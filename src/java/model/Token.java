package model;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
@Entity
@NamedQueries(
{
    @NamedQuery(name = "Token.findAll", query = "SELECT a FROM Token a"),
    @NamedQuery(name = "Token.findById", query = "SELECT a FROM Token a WHERE a.id = :id"),
    @NamedQuery(name = "Token.findByCodigo", query = "SELECT a FROM Token a WHERE a.codigo = :codigo"),
    @NamedQuery(name = "Token.findByHoraDeCriacao", query = "SELECT a FROM Token a WHERE a.horaDeCriacao = :horaDeCriacao"),
    @NamedQuery(name = "Token.findByUsuarioAssociado", query = "SELECT a FROM Token a WHERE a.usuarioAssociado = :usuarioAssociado")
})
public class Token implements Serializable
{    
    private static final long serialVersionUID = -8454495683594412531L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 100, nullable = false, unique = true)
    private String codigo;
    
    @OneToOne
    private Usuario usuarioAssociado;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar horaDeCriacao;

    public Token()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public Usuario getUsuarioAssociado()
    {
        return usuarioAssociado;
    }

    public void setUsuarioAssociado(Usuario usuarioAssociado)
    {
        this.usuarioAssociado = usuarioAssociado;
    }

    public Calendar getHoraDeCriacao()
    {
        return horaDeCriacao;
    }

    public void setHoraDeCriacao(Calendar horaDeCriacao)
    {
        this.horaDeCriacao = horaDeCriacao;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getCodigo() + "-" + getUsuarioAssociado();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Token)
        {
            Token token = (Token) obj;
            result = (token.getId() == null ? this.getId() == null : token.getId().equals(this.getId())) &&
                     (token.getCodigo() == null ? this.getCodigo() == null : token.getCodigo().equals(this.getCodigo())) &&
                     (token.getHoraDeCriacao() == null ? this.getHoraDeCriacao() == null : token.getHoraDeCriacao().equals(this.getHoraDeCriacao())) &&
                     (token.getUsuarioAssociado() == null ? this.getUsuarioAssociado() == null : token.getUsuarioAssociado().equals(this.getUsuarioAssociado()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}