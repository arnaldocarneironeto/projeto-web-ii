package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Chamado.findByEquipamento", query = "SELECT a FROM ChamadoDeEquipamento a WHERE a.equipamento = :equipamento")
})
public class ChamadoDeEquipamento extends Chamado implements Serializable
{
    private static final long serialVersionUID = -8318365297363026857L;
    
    @OneToOne
    private Equipamento equipamento;

    public Equipamento getEquipamento()
    {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento)
    {
        this.equipamento = equipamento;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getEquipamento() + "-" + getDescricaoDoProblema();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof ChamadoDeEquipamento)
        {
            ChamadoDeEquipamento chamado = (ChamadoDeEquipamento) obj;
            result = super.equals(this) &&
                     (chamado.getEquipamento() == null ? this.getEquipamento() == null : chamado.getEquipamento().equals(this.getEquipamento()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }

    @Override
    public boolean temEquipamentoAssociado()
    {
        return true;
    }
}