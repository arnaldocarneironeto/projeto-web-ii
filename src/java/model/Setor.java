package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Setor.findAll", query = "SELECT a FROM Setor a"),
    @NamedQuery(name = "Setor.findById", query = "SELECT a FROM Setor a WHERE a.id = :id"),
    @NamedQuery(name = "Setor.findByNome", query = "SELECT a FROM Setor a WHERE a.nome = :nome"),
    @NamedQuery(name = "Setor.findBySigla", query = "SELECT a FROM Setor a WHERE a.sigla = :sigla")
})
public class Setor implements Serializable
{
    private static final long serialVersionUID = -2393924680827103144L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;    
    
    @Column(length = 6, nullable = false, unique = true)
    private String sigla;

    @Column(length = 120, nullable = false, unique = true)
    private String nome;

    public Setor()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getSigla()
    {
        return sigla;
    }

    public void setSigla(String sigla)
    {
        this.sigla = sigla;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getSigla() + "-" + getNome();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Setor)
        {
            Setor setor = (Setor) obj;
            result = (setor.getId() == null ? this.getId() == null : setor.getId().equals(this.getId())) &&
                     (setor.getSigla() == null ? this.getSigla() == null : setor.getSigla().equals(this.getSigla())) &&
                     (setor.getNome() == null ? this.getNome() == null : setor.getNome().equals(this.getNome()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}