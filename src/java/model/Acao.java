package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Acao.findAll", query = "SELECT a FROM Acao a"),
    @NamedQuery(name = "Acao.findById", query = "SELECT a FROM Acao a WHERE a.id = :id"),
    @NamedQuery(name = "Acao.findByDescricaoDoQueDeveSerFeito", query = "SELECT a FROM Acao a WHERE a.descricaoDoQueDeveSerFeito = :descricaoDoQueDeveSerFeito"),
    @NamedQuery(name = "Acao.findByDescricaoDoQueFoiFeito", query = "SELECT a FROM Acao a WHERE a.descricaoDoQueFoiFeito = :descricaoDoQueFoiFeito"),
    @NamedQuery(name = "Acao.findByTecnico", query = "SELECT a FROM Acao a WHERE a.tecnico = :tecnico")
})
public class Acao implements Serializable
{
    private static final long serialVersionUID = 8843970619850824380L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    private Tecnico tecnico;

    @Column(nullable = false, length = 500)
    private String descricaoDoQueFoiFeito;

    @Column(length = 500)
    private String descricaoDoQueDeveSerFeito;

    public Acao()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Tecnico getTecnico()
    {
        return tecnico;
    }

    public void setTecnico(Tecnico tecnico)
    {
        this.tecnico = tecnico;
    }

    public String getDescricaoDoQueFoiFeito()
    {
        return descricaoDoQueFoiFeito;
    }

    public void setDescricaoDoQueFoiFeito(String descricaoDoQueFoiFeito)
    {
        this.descricaoDoQueFoiFeito = descricaoDoQueFoiFeito;
    }

    public String getDescricaoDoQueDeveSerFeito()
    {
        return descricaoDoQueDeveSerFeito;
    }

    public void setDescricaoDoQueDeveSerFeito(String descricaoDoQueDeveSerFeito)
    {
        this.descricaoDoQueDeveSerFeito = descricaoDoQueDeveSerFeito;
    }
    
    @Override
    public String toString()
    {
        return getId() + "-" + getTecnico() + "-" + getDescricaoDoQueFoiFeito() + "-" + getDescricaoDoQueDeveSerFeito();
    }

    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Acao)
        {
            Acao acao = (Acao) obj;
            result = (acao.getId() == null ? this.getId() == null : acao.getId().equals(this.getId())) &&
                     (acao.getDescricaoDoQueFoiFeito() == null ? this.getDescricaoDoQueFoiFeito() == null : acao.getDescricaoDoQueFoiFeito().equals(this.getDescricaoDoQueFoiFeito())) &&
                     (acao.getDescricaoDoQueDeveSerFeito() == null ? this.getDescricaoDoQueDeveSerFeito() == null : acao.getDescricaoDoQueDeveSerFeito().equals(this.getDescricaoDoQueDeveSerFeito())) &&
                     (acao.getTecnico() == null ? this.getTecnico() == null : acao.getTecnico().equals(this.getTecnico()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
}