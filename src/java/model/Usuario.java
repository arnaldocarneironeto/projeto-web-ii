package model;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@NamedQueries(
{
    @NamedQuery(name = "Usuario.findAll", query = "SELECT a FROM Usuario a"),
    @NamedQuery(name = "Usuario.findById", query = "SELECT a FROM Usuario a WHERE a.id = :id"),
    @NamedQuery(name = "Usuario.findByAtivo", query = "SELECT a FROM Usuario a WHERE a.ativo = :ativo"),
    @NamedQuery(name = "Usuario.findByDataDeCriacaoDaSenha", query = "SELECT a FROM Usuario a WHERE a.dataDeCriacaoDaSenha = :dataDeCriacaoDaSenha"),
    @NamedQuery(name = "Usuario.findByEmail", query = "SELECT a FROM Usuario a WHERE a.email = :email"),
    @NamedQuery(name = "Usuario.findByFoiAprovado", query = "SELECT a FROM Usuario a WHERE a.foiAprovado = :foiAprovado"),
    @NamedQuery(name = "Usuario.findByLogin", query = "SELECT a FROM Usuario a WHERE a.login = :login"),
    @NamedQuery(name = "Usuario.findByNome", query = "SELECT a FROM Usuario a WHERE a.nome = :nome"),
    @NamedQuery(name = "Usuario.findBySala", query = "SELECT a FROM Usuario a WHERE a.sala = :sala"),
    @NamedQuery(name = "Usuario.findBySalt", query = "SELECT a FROM Usuario a WHERE a.salt = :salt"),
    @NamedQuery(name = "Usuario.findBySenha", query = "SELECT a FROM Usuario a WHERE a.senha = :senha"),
    @NamedQuery(name = "Usuario.findBySetor", query = "SELECT a FROM Usuario a WHERE a.setor = :setor"),
    @NamedQuery(name = "Usuario.findByTelefone", query = "SELECT a FROM Usuario a WHERE a.telefone = :telefone")
})
public class Usuario implements Serializable
{
    private static final long serialVersionUID = 5019003951419637530L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 16, nullable = false, unique = true)
    private String login;

    @Column(length = 256, nullable = false)
    private String senha;

    @Column(length = 150, nullable = false)
    private String nome;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataDeCriacaoDaSenha;

    @Column(length = 256, nullable = false)
    private String salt;

    @Column(length = 15)
    private String telefone;

    @OneToOne
    private Setor setor;

    @Column(length = 7)
    private String sala;

    @Column(length = 30, nullable = false, unique = true)
    private String email;

    private boolean ativo;
    
    private boolean foiAprovado;
    
    @Transient
    private String tipo;

    public Usuario()
    {
        setTipo("usuario");
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getSenha()
    {
        return senha;
    }

    public void setSenha(String senha)
    {
        this.senha = senha;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public Calendar getDataDeCriacaoDaSenha()
    {
        return dataDeCriacaoDaSenha;
    }

    public void setDataDeCriacaoDaSenha(Calendar dataDeCriacaoDaSenha)
    {
        this.dataDeCriacaoDaSenha = dataDeCriacaoDaSenha;
    }

    public String getSalt()
    {
        return salt;
    }

    public void setSalt(String salt)
    {
        this.salt = salt;
    }

    public String getTelefone()
    {
        return telefone;
    }

    public void setTelefone(String telefone)
    {
        this.telefone = telefone;
    }

    public Setor getSetor()
    {
        return setor;
    }

    public void setSetor(Setor setor)
    {
        this.setor = setor;
    }

    public String getSala()
    {
        return sala;
    }

    public void setSala(String sala)
    {
        this.sala = sala;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean isAtivo()
    {
        return ativo;
    }

    public void setAtivo(boolean ativo)
    {
        this.ativo = ativo;
    }

    public boolean isFoiAprovado()
    {
        return foiAprovado;
    }

    public void setFoiAprovado(boolean foiAprovado)
    {
        this.foiAprovado = foiAprovado;
    }

    @Override
    public String toString()
    {
        return getId() + "-" + getLogin() + "-" + getNome();
    }

    /**
     * Faz a comparação dos atributos checando antes se os mesmos são nulos ou não
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj)
    {
        boolean result = false;
        if(obj != null && obj instanceof Usuario)
        {
            Usuario user = (Usuario) obj;
            result = user.isAtivo() == this.isAtivo() &&
                     user.isFoiAprovado() == this.isFoiAprovado() &&
                     (user.getId() == null ? this.getId() == null : user.getId().equals(this.getId())) &&
                     (user.getDataDeCriacaoDaSenha() == null ? this.getDataDeCriacaoDaSenha() == null : user.getDataDeCriacaoDaSenha().equals(this.getDataDeCriacaoDaSenha())) &&
                     (user.getEmail() == null ? this.getEmail() == null : user.getEmail().equals(this.getEmail())) &&
                     (user.getLogin() == null ? this.getLogin() == null : user.getLogin().equals(this.getLogin())) &&
                     (user.getNome() == null ? this.getNome() == null : user.getNome().equals(this.getNome())) &&
                     (user.getSala() == null ? this.getSala() == null : user.getSala().equals(this.getSala())) &&
                     (user.getSalt() == null ? this.getSalt() == null : user.getSalt().equals(this.getSalt())) &&
                     (user.getSenha() == null ? this.getSenha() == null : user.getSenha().equals(this.getSenha())) &&
                     (user.getSetor() == null ? this.getSetor() == null : user.getSetor().equals(this.getSetor())) &&
                     (user.getTelefone() == null ? this.getTelefone() == null : user.getTelefone().equals(this.getTelefone()));
        }
        return result;
    }

    @Override
    public int hashCode()
    {
        return toString().hashCode();
    }
    
    public String getTipo()
    {
        return "usuario";
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }
}