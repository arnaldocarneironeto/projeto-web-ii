package control;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Acao;
import model.Chamado;
import model.Estado;
import model.Tecnico;
import model.dao.ChamadoDao;
import model.dao.DaoFactory;
import model.dao.UsuarioDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class AtenderChamado extends HttpServlet
{
    private static final long serialVersionUID = -6099491132573281512L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        
        ChamadoDao cd = daoFactory.createChamadoDao();
        Chamado c = cd.find(new Long(request.getParameter("chamadoid")));
        
        UsuarioDao ud = daoFactory.createUsuarioDao();
        Tecnico t = (Tecnico) ud.find(request.getParameter("logintecnico"));
        
        boolean fechar = request.getParameter("fechar") != null && request.getParameter("fechar").equals("on");
        String oQueFoiFeito = request.getParameter("foifeito");
        String oQueFaltaFazer = request.getParameter("faltafazer");
        
        RequestDispatcher view = request.getRequestDispatcher("mensagem.jsp");
        if(fechar == true)
        {
            if(oQueFoiFeito.isEmpty() == false)
            {
                Acao a = new Acao();
                a.setDescricaoDoQueFoiFeito(oQueFoiFeito);
                a.setDescricaoDoQueDeveSerFeito(oQueFaltaFazer);
                a.setTecnico((Tecnico) session.getAttribute("usuario"));
                c.getListaDeAcoes().add(a);
                c.setTecnicoResponsavel((Tecnico) session.getAttribute("usuario"));
                c.setEstado(Estado.FECHADO);
                cd.save(c);
            }
            else
            {
                session.setAttribute("mensagem", "Só é possível fechar chamado se for informado o que foi feito.");
            }
        }
        else
        {
            if(oQueFoiFeito.isEmpty() == false && oQueFaltaFazer.isEmpty() == false)
            {
                Acao a = new Acao();
                a.setDescricaoDoQueFoiFeito(oQueFoiFeito);
                a.setDescricaoDoQueDeveSerFeito(oQueFaltaFazer);
                a.setTecnico((Tecnico) session.getAttribute("usuario"));
                c.getListaDeAcoes().add(a);
                if(t != null)
                {
                    c.setTecnicoResponsavel(t);
                }
                else
                {
                    c.setTecnicoResponsavel((Tecnico) session.getAttribute("usuario"));
                }
                cd.save(c);
                session.setAttribute("mensagem", "Chamado encaminhado com sucesso.");
            }
            else
            {
                session.setAttribute("mensagem", "Os campos informando o que foi feito e o que deve ser feito devem ser preenchidos antes de encaminhar o chamado.");
            }
        }
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}