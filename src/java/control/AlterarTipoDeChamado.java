package control;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.TipoDeChamado;
import model.dao.DaoFactory;
import model.dao.TipoDeChamadoDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class AlterarTipoDeChamado extends HttpServlet
{
    private static final long serialVersionUID = -3999049917318426316L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        
        String nome = request.getParameter("nome");
        String nomeantigo = request.getParameter("nomeantigo");
        
        TipoDeChamadoDao tdcd = daoFactory.createTipoDeChamadoDao();
        TipoDeChamado tdc = tdcd.find(nomeantigo);
        RequestDispatcher view;
        session.setAttribute("mensagem", "Falha ao alterar Tipo de Chamado.");
        view = request.getRequestDispatcher("mensagem.jsp");
        if(tdc != null)
        {
            tdc.setNome(nome);
            tdcd.save(tdc);
            session.setAttribute("mensagem", "Tipo de Chamado alterado com sucesso");
            view.forward(request, response);
        }
        else
        {
            view.forward(request, response);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}