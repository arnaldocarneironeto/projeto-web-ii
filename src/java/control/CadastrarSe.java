package control;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Setor;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.SetorDao;
import model.dao.UsuarioDao;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class CadastrarSe extends HttpServlet
{
    private static final long serialVersionUID = -6236635248129428177L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);

        String nome = request.getParameter("nome");
        String telefone = request.getParameter("telefone");
        Setor setor = findSetor(request.getParameter("setor"));
        String sala = request.getParameter("sala");
        String email = request.getParameter("email");
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        String confirmasenha = request.getParameter("confirmasenha");

        UsuarioDao ud = daoFactory.createUsuarioDao();
        Usuario usuario = ud.findByEmail(email);
        session.setAttribute("mensagem", "Falha ao cadastrar Usuário.");
        RequestDispatcher view;
        view = request.getRequestDispatcher("mensagem.jsp");
        if(usuario == null)
        {
            
            usuario = ud.find(login);
            if(usuario == null)
            {
                if(email != null && login != null && email.isEmpty() == false && login.isEmpty() == false)
                {
                    if(senha != null && senha.equals(confirmasenha))
                    {
                        usuario = new Usuario();
                        usuario.setAtivo(false);
                        usuario.setFoiAprovado(false);
                        processaSenha(usuario, senha);
                        usuario.setEmail(email);
                        usuario.setLogin(login);
                        usuario.setNome(nome);
                        usuario.setSala(sala);
                        usuario.setSetor(setor);
                        usuario.setTelefone(telefone);
                        ud.save(usuario);
                        session.setAttribute("mensagem", "Seu cadastro foi realizado com sucesso.");
                    }
                    else
                    {
                        session.setAttribute("mensagem", "A senha digitada é nula ou não conferem.");
                    }
                }
                else
                {
                    session.setAttribute("mensagem", "O e-mail ou o login estão em branco.");
                }
            }
            else
            {
                session.setAttribute("mensagem", "Já existe Usuário com este login.");
            }
        }
        else
        {
            session.setAttribute("mensagem", "Já existe Usuário com este e-mail.");
        }
        view.forward(request, response);
    }

    private void processaSenha(Usuario usuario, String senha)
    {
        usuario.setDataDeCriacaoDaSenha(Calendar.getInstance());
        usuario.setSalt(IdGenerator.generateId());
        usuario.setSenha(senha);
    }

    private Setor findSetor(String sigla)
    {
        SetorDao sd = daoFactory.createSetorDao();
        Setor result = sd.find(sigla);
        if(result == null)
        {
            result = new Setor();
            result.setSigla(sigla);
            result.setNome(sigla);
            sd.save(result);
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}