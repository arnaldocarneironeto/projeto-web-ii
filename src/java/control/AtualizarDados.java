package control;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Setor;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.SetorDao;
import model.dao.UsuarioDao;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class AtualizarDados extends HttpServlet
{
    private static final long serialVersionUID = 398902304316404296L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        
        SetorDao sd = daoFactory.createSetorDao();

        String nome = request.getParameter("nome");
        String telefone = request.getParameter("telefone");
        Setor setor = findSetor(sd, request);
        String sala = request.getParameter("sala");
        String email = request.getParameter("email");
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
        String novasenha = request.getParameter("novasenha");
        String confirmasenha = request.getParameter("confirmasenha");
        
        UsuarioDao ud = daoFactory.createUsuarioDao();
        Usuario u = ud.find(login);
        
        RequestDispatcher view;
        session.setAttribute("mensagem", "Falha ao atualizar Usuário.");
        view = request.getRequestDispatcher("mensagem.jsp");
        
        if(isValid(senha, u))
        {
            String msg = "";
            if(novasenha.isEmpty() == false)
            {
                msg = alteraSenha(novasenha, confirmasenha, u, msg);
            }
            u.setEmail(email);
            u.setNome(nome);
            u.setSala(sala);
            u.setSetor(setor);
            u.setTelefone(telefone);
            
            ud.save(u);
            session.setAttribute("mensagem", msg + "Usuário atualizado com sucesso");
        }
        else
        {
            session.setAttribute("mensagem", "Senha inválida ou não informada");
        }
        view.forward(request, response);
    }

    private Setor findSetor(SetorDao sd, HttpServletRequest request)
    {
        Setor setor;
        setor = sd.find(request.getParameter("setor"));
        if(setor == null)
        {
            setor = new Setor();
            setor.setSigla(request.getParameter("setor"));
            setor.setNome(request.getParameter("setor"));
            sd.save(setor);
        }
        return setor;
    }

    private String alteraSenha(String novasenha, String confirmasenha, Usuario u, String msg)
    {
        if(novasenha.equals(confirmasenha))
        {
            u.setSalt(IdGenerator.generateId());
            u.setDataDeCriacaoDaSenha(Calendar.getInstance());
            u.setSenha(novasenha);
            msg += "Senha alterada com sucesso. ";
        }
        else
        {
            msg += "Nova senha não confere. ";
        }
        return msg;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private boolean isValid(String senha, Usuario u)
    {
        boolean result = false;
        if(senha != null && senha.equals(u.getSenha()))
        {
            result = true;
        }
        return result;
    }
}