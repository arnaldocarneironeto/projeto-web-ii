package control;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Token;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.TokenDao;
import model.dao.UsuarioDao;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class AlteraSenha extends HttpServlet
{
    private static final long serialVersionUID = -3939841955532349016L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);

        String tkString = request.getParameter("token");
        String novasenha = request.getParameter("newpass");
        String confnovasenha = request.getParameter("newpassconf");
        int maxTime = Integer.parseInt(getServletContext().getInitParameter("maxTokenTime"));
        
        TokenDao td = daoFactory.createTokenDao();        
        Token token = td.find(tkString);
        
        RequestDispatcher view;
        if(isValidToken(token, maxTime) == true && novasenha.equals(confnovasenha) && isValidPassword(novasenha))
        {
            changePassword(token.getUsuarioAssociado(), novasenha);
            dispose(token);
            view = request.getRequestDispatcher("sucesso.jsp");
        }
        else
        {
            view = request.getRequestDispatcher("erroToken2.jsp");
        }
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }

    private boolean isValidToken(Token token, int maxAllowedTime)
    {
        boolean result = false;
        if(token != null)
        {
            Calendar c = Calendar.getInstance();
            long diff = (c.getTimeInMillis() - token.getHoraDeCriacao().getTimeInMillis()) / (60 * 1000);
            result = diff < maxAllowedTime;
        }
        return result;
    }

    private void changePassword(Usuario usuarioAssociado, String novasenha)
    {
        UsuarioDao ud = daoFactory.createUsuarioDao();
        Usuario usuario = ud.find(usuarioAssociado.getLogin());
        
        //Falta gerenciar melhor as senhas
        usuario.setSalt(IdGenerator.generateId());
        usuario.setSenha(novasenha);
        usuario.setDataDeCriacaoDaSenha(Calendar.getInstance());
        
        ud.save(usuario);
    }

    private boolean isValidPassword(String novasenha)
    {
        boolean result = true;
        if(novasenha != null)
        {
            result = novasenha.length() >= 1;
        }
        return result;
    }

    private void dispose(Token token)
    {
        TokenDao td = daoFactory.createTokenDao();
        td.delete(token);
    }
}