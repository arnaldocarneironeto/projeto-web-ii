package control;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Chamado;
import model.ChamadoDeEquipamento;
import model.Equipamento;
import model.Estado;
import model.TipoDeChamado;
import model.Usuario;
import model.dao.ChamadoDao;
import model.dao.DaoFactory;
import model.dao.EquipamentoDao;
import model.dao.TipoDeChamadoDao;
import model.dao.UsuarioDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class CadastrarChamado extends HttpServlet
{
    private static final long serialVersionUID = 2960526733585346414L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        
        TipoDeChamado tdc = getTipoDeChamado(request.getParameter("tipo"));
        Usuario u = getUsuario(request.getParameter("nome"));
        String tombamento = request.getParameter("tombamento");
        Equipamento e = getEquipamento(tombamento);
        String descricao = request.getParameter("descricao");
        RequestDispatcher view = request.getRequestDispatcher("mensagem.jsp");
        session.setAttribute("mensagem", "Não foi possível cadastrar o Chamado.");
        if(tdc != null && u != null && descricao != null && descricao.isEmpty() == false)
        {
            Chamado c;
            if(e != null)
            {
                c = new ChamadoDeEquipamento();
                ((ChamadoDeEquipamento)c).setEquipamento(e);
            }
            else
            {
                c = new Chamado();
            }
            c.setDescricaoDoProblema(descricao);
            c.setEstado(Estado.ABERTO);
            c.setListaDeAcoes(new ArrayList<>());
            c.setTecnicoResponsavel(null);
            c.setTipo(tdc);
            c.setUsuario(u);
            
            ChamadoDao cd = daoFactory.createChamadoDao();
            cd.save(c);
            session.setAttribute("mensagem", "Chamado cadastrado com sucesso.");
        }
        else
        {
            session.setAttribute("mensagem", "Não foi possível cadastrar o Chamado.\nFaltam informações.");
        }
        view.forward(request, response);
    }

    private TipoDeChamado getTipoDeChamado(String tipoStr)
    {
        TipoDeChamadoDao tdcd = daoFactory.createTipoDeChamadoDao();
        TipoDeChamado tdc = tdcd.find(new Long(tipoStr));
        return tdc;
    }

    private Equipamento getEquipamento(String tombamento)
    {
        Equipamento result;
        EquipamentoDao ed = daoFactory.createEquipamentoDao();
        result = ed.find(tombamento);
        return result;
    }

    private Usuario getUsuario(String login)
    {
        Usuario result;
        UsuarioDao ud = daoFactory.createUsuarioDao();
        result = ud.find(login);
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>
}