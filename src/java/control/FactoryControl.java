package control;

import model.dao.DaoFactory;
import model.dao.File.FileFactory;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class FactoryControl
{
    private static final DaoFactory daoFactory = new FileFactory();
    public static DaoFactory getDaoFactory()
    {
        return daoFactory;
    }
}