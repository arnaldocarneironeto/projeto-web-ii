package control;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Token;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.TokenDao;
import model.dao.UsuarioDao;
import utils.EmailUtility;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class RecuperarSenha extends HttpServlet
{
    private static final long serialVersionUID = 6991760532625877935L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);

        /**
         * 1 - email informado presente no banco de dados?
         *      1.1 - apaga tokens referentes a este email
         *      1.2 - cria novo token para este usuário
         *      1.3 - envia email com o link+token para o e-mail informado
         * 2 - envia o usuário para o jsp de resposta
         */
        
        String emailInformado = request.getParameter("email");
        
        if(isOnDatabase(emailInformado) == true)
        {
            Usuario usuario = getUsuario(emailInformado);
            erasePreviousTokens(usuario);
            boolean tokenWasCreatedCorrectly;
            Token token;
            do
            {
                token = new Token();
                token.setCodigo(IdGenerator.generateId());
                token.setHoraDeCriacao(Calendar.getInstance());
                token.setUsuarioAssociado(usuario);
                tokenWasCreatedCorrectly = addToken(token);
            }
            while(tokenWasCreatedCorrectly == false);
            sendMessage(emailInformado, token, request);
        }

        redirecionaJSP(request, response);
    }

    private void redirecionaJSP(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        RequestDispatcher view = request.getRequestDispatcher("recuperacaoDeSenhaEnviada.jsp");
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private boolean isOnDatabase(String emailInformado)
    {
        return getUsuario(emailInformado) != null;
    }

    private Usuario getUsuario(String emailInformado)
    {
        UsuarioDao ud = daoFactory.createUsuarioDao();
        return ud.findByEmail(emailInformado);
    }

    private void erasePreviousTokens(Usuario usuario)
    {
        if(usuario != null)
        {
            TokenDao td = daoFactory.createTokenDao();

            List<Token> list = td.findByUserId(usuario.getId());
            for(Token token: list)
            {
                td.delete(token);
            }
        }
    }

    private void sendMessage(String emailInformado, Token token, HttpServletRequest request)
    {
        String host = "smtp.gmail.com";
        String port = "465";
        String userName = getServletContext().getInitParameter("appEmailAcc");
        String password = getServletContext().getInitParameter("appEmailPwd");
        String subject = "Endereço para recuperação da senha em " + getServletContext().getInitParameter("appName");
        String appPath = "http://" + request.getServerName() + ":" + request.getServerPort() + getServletContext().getContextPath();
        String message = "Recentemente você ou alguma outra pessoa solicitou uma nova senha no nosso sistema.\n\n" +
                         "O link para criação de uma nova senha é\n" +
                         appPath + "/NovaSenha?prid=" + token.getCodigo() + "\n\n" +
                         "Caso você não tenha solicitado uma nova senha, apenas desconsidere esta mensagem.";
        if(EmailUtility.sendEmail(host, port, userName, password, emailInformado, subject, message) == true)
        {
            System.out.println("Operação ok!");
        }
        else
        {
            System.err.println("Operação falhou!");
        }
    }

    private boolean addToken(Token token)
    {
        boolean result;
        TokenDao td = daoFactory.createTokenDao();
        td.save(token);
        result = true;
        return result;
    }
}