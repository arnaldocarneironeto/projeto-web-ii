package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Token;
import model.dao.DaoFactory;
import model.dao.TokenDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class NovaSenha extends HttpServlet
{
    private static final long serialVersionUID = -2977114989095172336L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        int maxTime = Integer.parseInt(getServletContext().getInitParameter("maxTokenTime"));
        
        /**
         * 1 - o token informado é válido? 1.1 - invalida o o token informado
         * 1.2 - mostra formulario para nova senha 1.3 - redireciona para o
         * servlet de alteração de senha 2 - redireciona para o jsp de servlet
         * invalido
         */
        String tkString = request.getParameter("prid");
        Token token = getTokenByCode(tkString);
        if(isValidToken(token, maxTime) == true)
        {
            changePassword(response, token);
        }
        else
        {
            invalidToken(request, response);
        }
    }

    private void invalidToken(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
//        request.setAttribute("message", "Token inválido.");
        RequestDispatcher view = request.getRequestDispatcher("erroToken1.jsp");
        view.forward(request, response);
    }

    private void changePassword(HttpServletResponse response, Token token) throws IOException, ServletException
    {
        response.setContentType("text/html;charset=UTF-8");
        try(PrintWriter out = response.getWriter())
        {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>" + getServletContext().getInitParameter("appName") + "</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>" + getServletContext().getInitParameter("appName") + "</h1>");
            out.println("<form method=\"POST\" action=\"AlteraSenha\">");
            out.println("<fieldset>");
            out.println("<label for=\"newpass\">Nova Senha:</label>");
            out.println("<input type=\"password\" name=\"newpass\" id=\"newpass\" />");
            out.println("<label for=\"newpassconf\">Confirma Nova Senha:</label>");
            out.println("<input type=\"password\" name=\"newpassconf\" id=\"newpassconf\" />");
            out.println("<input type=\"hidden\" name=\"token\" value=\"" + token.getCodigo() + "\">");
            out.println("<input type=\"submit\" value=\"Alterar senha\"/>");
            out.println("</fieldset>");
            out.println("</form>");
            out.println("<p><a href=\"" + getServletContext().getContextPath() +"\">Voltar</a></p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private boolean isValidToken(Token token, int maxAllowedTime)
    {
        boolean result = false;
        if(token != null)
        {
            Calendar c = Calendar.getInstance();
            long diff = (c.getTimeInMillis() - token.getHoraDeCriacao().getTimeInMillis()) / (60 * 1000);
            System.err.println("Diferença = " + diff);
            result = diff < maxAllowedTime;
        }
        return result;
    }
    
    private Token getTokenByCode(String tkString)
    {
        TokenDao td = daoFactory.createTokenDao();
        return td.find(tkString);
    }
}