package control;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.UsuarioDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Login extends HttpServlet
{
    private static final long serialVersionUID = -5793613620224479825L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();
        
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);

        String login = request.getParameter("login");
        String senha = request.getParameter("senha");
                
        RequestDispatcher view;
        if(isValidUser(login, senha) == true)
        {
            session = request.getSession(true);
            session.setAttribute("usuario", getUser(login));
            view = request.getRequestDispatcher("telaprincipal.jsp");
        }
        else
        {
            if(session != null)
            {
                session.invalidate();
            }
            view = request.getRequestDispatcher("loginerrado.jsp");
        }
        view.forward(request, response);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private boolean isValidUser(String login, String senha)
    {
        Usuario usuario = getUser(login);
        return usuario != null && usuario.getSenha().equals(senha) && usuario.isAtivo() && usuario.isFoiAprovado();
    }

    private Usuario getUser(String login)
    {
        UsuarioDao ud = daoFactory.createUsuarioDao();
        return ud.find(login);
    }
}