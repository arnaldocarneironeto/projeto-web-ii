package control;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.EmailUtility;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class EnviarMensagem extends HttpServlet
{
    private static final long serialVersionUID = -2427538668459758519L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);

        String nome = request.getParameter("nome");
        String emailparacontato = request.getParameter("emailparacontato");
        String assunto = request.getParameter("assunto");
        String mensagem = request.getParameter("mensagem");
        Boolean wasTheMessageSent;
        wasTheMessageSent = sendMessage(nome, emailparacontato, assunto, mensagem, getServletContext().getInitParameter("appName"));
        
        request.setAttribute("wasTheMessageSent", wasTheMessageSent);
        RequestDispatcher view = request.getRequestDispatcher("mensagemEnviada.jsp");
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private boolean sendMessage(String nome, String emailParaContato, String assunto, String mensagem, String nomeDaAplicacao)
    {
        String host = "smtp.gmail.com";
        String port = "465";
        String userName = getServletContext().getInitParameter("appEmailAcc");
        String password = getServletContext().getInitParameter("appEmailPwd");
        String message = "Nome:\t" + nome + "\n"
                         + "E-mail para contato:\t" + emailParaContato + "\n"
                         + "Assunto:\t" + assunto + "\n\n"
                         + "\t" + mensagem;
        return EmailUtility.sendEmail(host, port, userName, password, userName + "@gmail.com", "[" + nomeDaAplicacao + "]" + assunto, message);
    }
}