package control;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Usuario;
import model.dao.DaoFactory;
import model.dao.UsuarioDao;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class Autorizar extends HttpServlet
{
    private static final long serialVersionUID = 1356132596872277627L;
    private final DaoFactory daoFactory = FactoryControl.getDaoFactory();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession(false);
        
        Enumeration<String> boxenames = request.getParameterNames();
        
        UsuarioDao ud = daoFactory.createUsuarioDao();
        String msg = "";
        while(boxenames.hasMoreElements())
        {
            String login = boxenames.nextElement().substring(2);
            Usuario u = ud.find(login);
            u.setFoiAprovado(true);
            u.setAtivo(true);
            ud.save(u);
            msg += "Usuário " + u.getLogin() + " foi autorizado e agora está ativo.\n";
        }
//        removeUnauthorizedUsers(ud);
        
        RequestDispatcher view = request.getRequestDispatcher("mensagem.jsp");
        session.setAttribute("mensagem", msg);
        
        view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Short description";
    }// </editor-fold>

    private void removeUnauthorizedUsers(UsuarioDao ud)
    {
        List<Usuario> list = ud.list();
        for(Usuario u: list)
        {
            if(u.isFoiAprovado() == false)
            {
                ud.delete(u);
            }
        }
    }
}