package utils;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * A utility class for sending e-mail messages
 *
 * @author www.codejava.net
 *
 */
public class EmailUtility
{
    public static boolean sendEmail(String host, String port,
                                    final String userName,
                                    final String password, String toAddress,
                                    String subject, String message)
    {
        boolean messageWasSent = false;
        try
        {
            Session session = getSession(host, port, userName, password);

            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(userName));
            InternetAddress[] toAddresses =
            {
                new InternetAddress(toAddress)
            };
            msg.setRecipients(Message.RecipientType.TO, toAddresses);
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setText(message);

            Transport.send(msg);

            messageWasSent = true;
        }
        catch(MessagingException ex)
        {
            Logger.getLogger(EmailUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return messageWasSent;
    }

    private static Session getSession(String host, String port, final String userName, final String password)
    {
        Properties properties = getProperties(host, port);
        Authenticator auth = getAuthenticator(userName, password);
        Session session = Session.getInstance(properties, auth);
        session.setDebug(true);
        return session;
    }

    private static Properties getProperties(String host, String port)
    {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.debug", "true");
//        properties.put("mail.smtp.debug", "true");
//        properties.put("mail.mime.charset", "ISO-8859-1");
        properties.put("mail.mime.charset", "UTF-8");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        return properties;
    }

    private static Authenticator getAuthenticator(final String userName, final String password)
    {
        Authenticator auth = new Authenticator()
        {
            @Override
            public PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(userName, password);
            }
        };
        return auth;
    }
}
