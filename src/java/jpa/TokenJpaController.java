package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Token;
import model.Usuario;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class TokenJpaController implements Serializable
{
    private static final long serialVersionUID = 4193915938355025692L;

    private static final List<Token> lista = new ArrayList<>();

    public TokenJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Token token) throws RollbackFailureException, Exception
    {
        Token t = findToken(token.getId());
        if(t == null)
        {
            token.setId(new Long(lista.size()));
            lista.add(token);
        }
        else
        {
            throw new EntityExistsException("Token já existe");
        }
    }

    public void edit(Token token) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Token t = findToken(token.getId());
        if(t != null)
        {
            t.setCodigo(token.getCodigo());
            t.setHoraDeCriacao(token.getHoraDeCriacao());
            t.setUsuarioAssociado(token.getUsuarioAssociado());
        }
        else
        {
            throw new NonexistentEntityException("Este Token não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Token t = findToken(id);
        if(t != null)
        {
            lista.remove(t);
        }
        else
        {
            throw new NonexistentEntityException("Este Token não existe");
        }
    }

    public List<Token> findTokenEntities()
    {
        return findTokenEntities(true, -1, -1);
    }

    public List<Token> findTokenEntities(int maxResults, int firstResult)
    {
        return findTokenEntities(false, maxResults, firstResult);
    }

    private List<Token> findTokenEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Token> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Token findToken(Long id)
    {
        Token result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getTokenCount()
    {
        return lista.size();

    }
//
//    public Token findTokenByCode(String tkString)
//    {
//        EntityManager em = getEntityManager();
//        try
//        {
//            TypedQuery<Token> query = em.createNamedQuery("Token.findByCodigo", Token.class);
//            query.setParameter("a", tkString);
//            List<Token> list = query.getResultList();
//            return list.get(0);
//        }
//        finally
//        {
//            em.close();
//        }
//    }
//    
//    public List<Token> findAssociatedTokenList(Usuario usuario)
//    {
//        EntityManager em = getEntityManager();
//        try
//        {
//            TypedQuery<Token> query = em.createNamedQuery("Token.findByUsuarioAssociado", Token.class);
//            query.setParameter("a", usuario);
//            List<Token> list = query.getResultList();
//            return list;
//        }
//        finally
//        {
//            em.close();
//        }
//    }

    public Token findTokenByCode(String tkString)
    {
        Token result = null;
        for(int i = 0; result == null && lista.get(i).getCodigo().equals(tkString); i++)
        {
            result = lista.get(i);
        }
        return result;
    }
    
    public List<Token> findAssociatedTokenList(Usuario usuario)
    {
        List<Token> result = new ArrayList<>();
        for(Token token : lista)
        {
            if(token.getUsuarioAssociado().equals(usuario))
            {
                result.add(token);
            }
        }
        return result;
    }
}