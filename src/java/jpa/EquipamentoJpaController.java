package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Equipamento;
import model.TipoDeEquipamento;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class EquipamentoJpaController implements Serializable
{
    private static final long serialVersionUID = -1713561314603726265L;
    
    private static final List<Equipamento> lista = new ArrayList<>();
    
    static
    {
        TipoDeEquipamentoJpaController tdejc = new TipoDeEquipamentoJpaController(null, null);
        
        Equipamento e;
        
        e = new Equipamento();
        e.setId(new Long("0"));
        e.setAtivo(true);
        e.setTipo(tdejc.findTipoDeEquipamentoByNome("Impressora"));
        e.setTombamento("TI2009005000");
        e.setDescricao("Kyocera");
        lista.add(e);
        
        e = new Equipamento();
        e.setId(new Long("1"));
        e.setAtivo(false);
        e.setTipo(tdejc.findTipoDeEquipamentoByNome("Monitor"));
        e.setTombamento("TI2010001247");
        e.setDescricao("Acer");
        lista.add(e);
    }

    public EquipamentoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Equipamento equipamento) throws RollbackFailureException, Exception
    {
        Equipamento e = findEquipamento(equipamento.getId());
        if(e == null)
        {
            equipamento.setId(new Long(lista.size()));
            lista.add(equipamento);
        }
        else
        {
            throw new EntityExistsException("Equipamento já existe");
        }
    }

    public void edit(Equipamento equipamento) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Equipamento e = findEquipamento(equipamento.getId());
        if(e != null)
        {
            e.setAtivo(equipamento.isAtivo());
            e.setDescricao(equipamento.getDescricao());
            e.setTipo(equipamento.getTipo());
            e.setTombamento(equipamento.getTombamento());
        }
        else
        {
            throw new NonexistentEntityException("Este Equipamento não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Equipamento e = findEquipamento(id);
        if(e != null)
        {
            lista.remove(e);
        }
        else
        {
            throw new NonexistentEntityException("Este Equipamento não existe");
        }
    }

    public List<Equipamento> findEquipamentoEntities()
    {
        return findEquipamentoEntities(true, -1, -1);
    }

    public List<Equipamento> findEquipamentoEntities(int maxResults, int firstResult)
    {
        return findEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<Equipamento> findEquipamentoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Equipamento> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Equipamento findEquipamento(Long id)
    {
        Equipamento result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    List<Equipamento> findEquipamentosByTipo(TipoDeEquipamento tde)
    {
        List<Equipamento> result = new ArrayList<>();
        for(Equipamento equipamento: lista)
        {
            if(equipamento.getTipo().equals(tde))
            {
                result.add(equipamento);
            }
        }
        return result;
    }
    
    public Equipamento findEquipamentoByTombamento(String tombamento)
    {
        Equipamento result = null;
        if(tombamento != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(tombamento.equals(lista.get(i).getTombamento()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getEquipamentoCount()
    {
        return lista.size();
    }
}