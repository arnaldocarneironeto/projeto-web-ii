package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.ChamadoDeEquipamento;
import model.Equipamento;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class ChamadoDeEquipamentoJpaController implements Serializable
{
    private static final long serialVersionUID = 5455104257913861743L;
    
    private static final List<ChamadoDeEquipamento> lista = new ArrayList<>();

    public ChamadoDeEquipamentoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(ChamadoDeEquipamento chamadoDeEquipamento) throws RollbackFailureException, Exception
    {
        ChamadoDeEquipamento cde = findChamadoDeEquipamento(chamadoDeEquipamento.getId());
        if(cde == null)
        {
            Equipamento equipamento = chamadoDeEquipamento.getEquipamento();
            if(equipamento != null)
            {
                chamadoDeEquipamento.setId(new Long(lista.size()));
                lista.add(chamadoDeEquipamento);
            }
            else
            {
                throw new NonexistentEntityException("Equipamento não existe");
            }
        }
        else
        {
            throw new EntityExistsException("Chamado de Equipamento já existe");
        }
    }

    public void edit(ChamadoDeEquipamento chamadoDeEquipamento) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        ChamadoDeEquipamento cde = findChamadoDeEquipamento(chamadoDeEquipamento.getId());
        if(cde != null)
        {
            Equipamento equipamento = chamadoDeEquipamento.getEquipamento();
            if(equipamento != null)
            {
                cde.setDescricaoDoProblema(chamadoDeEquipamento.getDescricaoDoProblema());
                cde.setEstado(chamadoDeEquipamento.getEstado());
                cde.setListaDeAcoes(chamadoDeEquipamento.getListaDeAcoes());
                cde.setTecnicoResponsavel(chamadoDeEquipamento.getTecnicoResponsavel());
                cde.setTipo(chamadoDeEquipamento.getTipo());
                cde.setUsuario(chamadoDeEquipamento.getUsuario());
                cde.setEquipamento(equipamento);
            }
            else
            {
                throw new NonexistentEntityException("Equipamento não existe");
            }
        }
        else
        {
            throw new NonexistentEntityException("Este Chamado de Equipamento não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        ChamadoDeEquipamento cde = findChamadoDeEquipamento(id);
        if(cde != null)
        {
            lista.remove(cde);
        }
        else
        {
            throw new NonexistentEntityException("Este Chamado de Equipamento não existe");
        }
    }

    public List<ChamadoDeEquipamento> findChamadoDeEquipamentoEntities()
    {
        return findChamadoDeEquipamentoEntities(true, -1, -1);
    }

    public List<ChamadoDeEquipamento> findChamadoDeEquipamentoEntities(int maxResults, int firstResult)
    {
        return findChamadoDeEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<ChamadoDeEquipamento> findChamadoDeEquipamentoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<ChamadoDeEquipamento> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public ChamadoDeEquipamento findChamadoDeEquipamento(Long id)
    {
        ChamadoDeEquipamento result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getChamadoDeEquipamentoCount()
    {
        return lista.size();
    }
}