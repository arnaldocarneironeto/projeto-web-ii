package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Equipamento;
import model.TipoDeEquipamento;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class TipoDeEquipamentoJpaController implements Serializable
{
    private static final long serialVersionUID = -4716912279404686523L;

    private static final List<TipoDeEquipamento> lista = new ArrayList<>();
    
    static
    {
        TipoDeEquipamento tde;
        
        tde = new TipoDeEquipamento();
        tde.setId(new Long("0"));
        tde.setNome("Impressora");
        lista.add(tde);
        
        tde = new TipoDeEquipamento();
        tde.setId(new Long("1"));
        tde.setNome("Monitor");
        lista.add(tde);
        
        tde = new TipoDeEquipamento();
        tde.setId(new Long("2"));
        tde.setNome("Notebook");
        lista.add(tde);
    }
    
    public TipoDeEquipamentoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(TipoDeEquipamento tipoDeEquipamento) throws RollbackFailureException, Exception
    {
        TipoDeEquipamento tde = findTipoDeEquipamento(tipoDeEquipamento.getId());
        if(tde == null)
        {
            tipoDeEquipamento.setId(new Long(lista.size()));
            lista.add(tipoDeEquipamento);
        }
        else
        {
            throw new EntityExistsException("Tipo de Equipamento já existe");
        }
    }

    public void edit(TipoDeEquipamento tipoDeEquipamento) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        TipoDeEquipamento tde = findTipoDeEquipamento(tipoDeEquipamento.getId());
        if(tde != null)
        {
            tde.setNome(tipoDeEquipamento.getNome());
        }
        else
        {
            throw new NonexistentEntityException("Este Tipo de Equipamento não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        TipoDeEquipamento tde = findTipoDeEquipamento(id);
        if(tde != null)
        {
            EquipamentoJpaController ejc = new EquipamentoJpaController(utx, emf);
            List<Equipamento> list = ejc.findEquipamentosByTipo(tde);
            if(list.isEmpty() == false)
            {
                List<String> strings = new ArrayList<>();
                for(Equipamento equipamento: list)
                {
                    strings.add("Este Equipamento ficará orfão " + equipamento);
                }
                throw new IllegalOrphanException(strings);
            }
            lista.remove(tde);
        }
        else
        {
            throw new NonexistentEntityException("Este Tipo de Equipamento não existe");
        }
    }

    public List<TipoDeEquipamento> findTipoDeEquipamentoEntities()
    {
        return findTipoDeEquipamentoEntities(true, -1, -1);
    }

    public List<TipoDeEquipamento> findTipoDeEquipamentoEntities(int maxResults, int firstResult)
    {
        return findTipoDeEquipamentoEntities(false, maxResults, firstResult);
    }

    private List<TipoDeEquipamento> findTipoDeEquipamentoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<TipoDeEquipamento> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public TipoDeEquipamento findTipoDeEquipamento(Long id)
    {
        TipoDeEquipamento result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getTipoDeEquipamentoCount()
    {
        return lista.size();
    }

    public TipoDeEquipamento findTipoDeEquipamentoByNome(String nomeantigo)
    {
        TipoDeEquipamento result = null;
        if(nomeantigo != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(nomeantigo.equals(lista.get(i).getNome()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }
}