package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Acao;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class AcaoJpaController implements Serializable
{
    private static final long serialVersionUID = 2628032015220594039L;
    
    private static final List<Acao> lista = new ArrayList<>();

    public AcaoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Acao acao) throws RollbackFailureException, Exception
    {
        Acao ac = findAcao(acao.getId());
        if(ac == null)
        {
            acao.setId(new Long(lista.size()));
            lista.add(acao);
        }
        else
        {
            throw new EntityExistsException("Ação já existe");
        }
    }

    public void edit(Acao acao) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Acao ac = findAcao(acao.getId());
        if(ac != null)
        {
            ac.setTecnico(acao.getTecnico());
            ac.setDescricaoDoQueFoiFeito(acao.getDescricaoDoQueFoiFeito());
            ac.setDescricaoDoQueDeveSerFeito(acao.getDescricaoDoQueDeveSerFeito());
        }
        else
        {
            throw new NonexistentEntityException("Esta Ação não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Acao ac = findAcao(id);
        if(ac != null)
        {
            lista.remove(ac);
        }
        else
        {
            throw new NonexistentEntityException("Esta Ação não existe");
        }
    }

    public List<Acao> findAcaoEntities()
    {
        return findAcaoEntities(true, -1, -1);
    }

    public List<Acao> findAcaoEntities(int maxResults, int firstResult)
    {
        return findAcaoEntities(false, maxResults, firstResult);
    }

    private List<Acao> findAcaoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Acao> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Acao findAcao(Long id)
    {
        Acao result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getAcaoCount()
    {
        return lista.size();
    }
}