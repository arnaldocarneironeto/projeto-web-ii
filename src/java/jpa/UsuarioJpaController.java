package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Administrador;
import model.Setor;
import model.Tecnico;
import model.Usuario;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;
import utils.IdGenerator;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class UsuarioJpaController implements Serializable
{
    private static final long serialVersionUID = 2707972168071056772L;

    private static final List<Usuario> lista = new ArrayList<>();
    
    static
    {
        Setor s = new Setor();
        s.setSigla("IT");
        s.setNome("Setor de Informática");
        SetorJpaController sjc = new SetorJpaController(null, null);
        try
        {
            sjc.create(s);
        }
        catch(Exception ex)
        {
            Logger.getLogger(UsuarioJpaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Usuario u = new Usuario();
        u.setAtivo(true);
        u.setDataDeCriacaoDaSenha(Calendar.getInstance());
        u.setEmail("ren.gsg@gmail.com");
        u.setFoiAprovado(true);
        u.setId(00L);
        u.setLogin("user");
        u.setNome("Renato");
        u.setSala("340");
        u.setSalt(IdGenerator.generateId());
        u.setSenha("user");
        u.setSetor(s);
        u.setTelefone("2126-7054");
        lista.add(u);
        
        u = new Tecnico();
        u.setAtivo(true);
        u.setDataDeCriacaoDaSenha(Calendar.getInstance());
        u.setEmail("gaucho.rpg@gmail.com");
        u.setFoiAprovado(true);
        u.setId(01L);
        u.setLogin("tech");
        u.setNome("Diogo");
        u.setSala("340");
        u.setSalt(IdGenerator.generateId());
        u.setSenha("tech");
        u.setSetor(s);
        u.setTelefone("2126-7054");
        lista.add(u);
        
        u = new Administrador();
        u.setAtivo(true);
        u.setDataDeCriacaoDaSenha(Calendar.getInstance());
        u.setEmail("arnymal@gmail.com");
        u.setFoiAprovado(true);
        u.setId(02L);
        u.setLogin("admin");
        u.setNome("Arnaldo");
        u.setSala("-");
        u.setSalt(IdGenerator.generateId());
        u.setSenha("admin");
        u.setSetor(s);
        u.setTelefone("3535-6401");
        lista.add(u);
        
        u = new Usuario();
        u.setAtivo(false);
        u.setDataDeCriacaoDaSenha(Calendar.getInstance());
        u.setEmail("alguem@algumlugar.com");
        u.setFoiAprovado(false);
        u.setId(03L);
        u.setLogin("alguem");
        u.setNome("Alguém");
        u.setSala("-");
        u.setSalt(IdGenerator.generateId());
        u.setSenha("alguem");
        u.setSetor(s);
        u.setTelefone("555-5555");
        lista.add(u);
        
        u = new Usuario();
        u.setAtivo(false);
        u.setDataDeCriacaoDaSenha(Calendar.getInstance());
        u.setEmail("outroalguem@algumlugar.com");
        u.setFoiAprovado(false);
        u.setId(04L);
        u.setLogin("outroalguem");
        u.setNome("Outro Alguém");
        u.setSala("-");
        u.setSalt(IdGenerator.generateId());
        u.setSenha("outroalguem");
        u.setSetor(s);
        u.setTelefone("555-6666");
        lista.add(u);
    }
    
    public UsuarioJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws RollbackFailureException, Exception
    {
        Usuario u = findUsuario(usuario.getId());
        if(u == null)
        {
            usuario.setId(new Long(lista.size()));
            lista.add(usuario);
        }
        else
        {
            throw new EntityExistsException("Usuário já existe");
        }
    }

    public void edit(Usuario usuario) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Usuario u = findUsuario(usuario.getId());
        if(u != null)
        {
            u.setAtivo(usuario.isAtivo());
            u.setFoiAprovado(usuario.isFoiAprovado());
            u.setDataDeCriacaoDaSenha(usuario.getDataDeCriacaoDaSenha());
            u.setEmail(usuario.getEmail());
            u.setLogin(usuario.getLogin());
            u.setNome(usuario.getNome());
            u.setSala(usuario.getSala());
            u.setSalt(usuario.getSalt());
            u.setSenha(usuario.getSenha());
            u.setSetor(usuario.getSetor());
            u.setTelefone(usuario.getTelefone());
        }
        else
        {
            throw new NonexistentEntityException("Este Usuário não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Usuario u = findUsuario(id);
        if(u != null)
        {
            lista.remove(u);
        }
        else
        {
            throw new NonexistentEntityException("Este Usuário não existe");
        }
    }

    public List<Usuario> findUsuarioEntities()
    {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult)
    {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Usuario> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Usuario findUsuario(Long id)
    {
        Usuario result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }
//    
//    public Usuario findUsuarioByLogin(String login)
//    {
//        EntityManager em = getEntityManager();
//        try
//        {
//            TypedQuery<Usuario> query = em.createNamedQuery("Usuario.findByLogin", Usuario.class);
//            query.setParameter("a", login);
//            List<Usuario> list = query.getResultList();
//            Usuario usuario = list.get(0);
//            return usuario;
//        }
//        finally
//        {
//            em.close();
//        }
//    }
//    
//    public Usuario findUsuarioByEmail(String email)
//    {
//        EntityManager em = getEntityManager();
//        try
//        {
//            TypedQuery<Usuario> query = em.createNamedQuery("Usuario.findByEmail", Usuario.class);
//            query.setParameter("a", email);
//            List<Usuario> list = query.getResultList();
//            Usuario usuario = list.get(0);
//            return usuario;
//        }
//        finally
//        {
//            em.close();
//        }
//    }
    
    public Usuario findUsuarioByLogin(String login)
    {
        Usuario result = null;
        if(login != null && login.isEmpty() == false)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                String l = lista.get(i).getLogin();
                if(login.equals(lista.get(i).getLogin()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }
    
    public Usuario findUsuarioByEmail(String email)
    {
        Usuario result = null;
        if(email != null && email.isEmpty() == false)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(email.equals(lista.get(i).getEmail()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getUsuarioCount()
    {
        return lista.size();
    }
}