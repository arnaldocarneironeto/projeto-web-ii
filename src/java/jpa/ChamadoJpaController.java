package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Chamado;
import model.ChamadoDeEquipamento;
import model.Estado;
import model.TipoDeChamado;
import model.Usuario;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class ChamadoJpaController implements Serializable
{
    private static final long serialVersionUID = -8327762403483331769L;
    
    private static final List<Chamado> lista = new ArrayList<>();
    
    static
    {
        EquipamentoJpaController ejc = new EquipamentoJpaController(null, null);
        TipoDeChamadoJpaController tdcjc = new TipoDeChamadoJpaController(null, null);
        UsuarioJpaController ujc = new UsuarioJpaController(null, null);
        
        Chamado c;
        
        c = new Chamado();
        c.setId(new Long("0"));
        c.setEstado(Estado.ABERTO);
        c.setDescricaoDoProblema("O mouse sem fio não está funcionando direito.");
        c.setListaDeAcoes(new ArrayList<>());
        c.setTipo(tdcjc.findTipoDeChamadoByNome("Outros"));
        c.setUsuario(ujc.findUsuarioByLogin("user"));
        lista.add(c);
        
        c = new ChamadoDeEquipamento();
        ((ChamadoDeEquipamento) c).setEquipamento(ejc.findEquipamentoByTombamento("TI2009005000"));
        c.setId(new Long("1"));
        c.setEstado(Estado.ABERTO);
        c.setDescricaoDoProblema("O toner está no fim.");
        c.setListaDeAcoes(new ArrayList<>());
        c.setTipo(tdcjc.findTipoDeChamadoByNome("Troca de toner"));
        c.setUsuario(ujc.findUsuarioByLogin("admin"));
        lista.add(c);
    }

    public ChamadoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Chamado chamado) throws RollbackFailureException, Exception
    {
        Chamado c = findChamado(chamado.getId());
        if(c == null)
        {
            chamado.setId(new Long(lista.size()));
            lista.add(chamado);
        }
        else
        {
            throw new EntityExistsException("Ação já existe");
        }
    }

    public void edit(Chamado chamado) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Chamado c = findChamado(chamado.getId());
        if(c != null)
        {
            c.setDescricaoDoProblema(chamado.getDescricaoDoProblema());
            c.setEstado(chamado.getEstado());
            c.setListaDeAcoes(chamado.getListaDeAcoes());
            c.setTecnicoResponsavel(chamado.getTecnicoResponsavel());
            c.setTipo(chamado.getTipo());
            c.setUsuario(chamado.getUsuario());
        }
        else
        {
            throw new NonexistentEntityException("Este Chamado não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Chamado c = findChamado(id);
        if(c != null)
        {
            lista.remove(c);
        }
        else
        {
            throw new NonexistentEntityException("Este Chamado não existe");
        }
    }

    public List<Chamado> findChamadoEntities()
    {
        return findChamadoEntities(true, -1, -1);
    }

    public List<Chamado> findChamadoEntities(int maxResults, int firstResult)
    {
        return findChamadoEntities(false, maxResults, firstResult);
    }

    private List<Chamado> findChamadoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Chamado> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Chamado findChamado(Long id)
    {
        Chamado result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public  List<Chamado> findChamadosByTipo(TipoDeChamado tdc)
    {
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado: lista)
        {
            if(chamado.getTipo().equals(tdc))
            {
                result.add(chamado);
            }
        }
        return result;
    }
    
    public List<Chamado> findChamadosByUsuario(Usuario usuario)
    {
        List<Chamado> result = new ArrayList<>();
        for(Chamado chamado: lista)
        {
            if(chamado.getUsuario().equals(usuario))
            {
                result.add(chamado);
            }
        }
        return result;
    }

    public int getChamadoCount()
    {
        return lista.size();
    }
}