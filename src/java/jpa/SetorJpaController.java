package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Setor;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class SetorJpaController implements Serializable
{
    private static final long serialVersionUID = 7445646396737890356L;

    private static final List<Setor> lista = new ArrayList<>();
     
    public SetorJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(Setor setor) throws RollbackFailureException, Exception
    {
        Setor s = findSetor(setor.getId());
        if(s == null)
        {
            setor.setId(new Long(lista.size()));
            lista.add(setor);
        }
        else
        {
            throw new EntityExistsException("Setor já existe");
        }
    }

    public void edit(Setor setor) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Setor s = findSetor(setor.getId());
        if(s != null)
        {
            s.setSigla(setor.getSigla());
            s.setNome(setor.getNome());
        }
        else
        {
            throw new NonexistentEntityException("Este Setor não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        Setor s = findSetor(id);
        if(s != null)
        {
            lista.remove(s);
        }
        else
        {
            throw new NonexistentEntityException("Este Setor não existe");
        }
    }

    public List<Setor> findSetorEntities()
    {
        return findSetorEntities(true, -1, -1);
    }

    public List<Setor> findSetorEntities(int maxResults, int firstResult)
    {
        return findSetorEntities(false, maxResults, firstResult);
    }

    private List<Setor> findSetorEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<Setor> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public Setor findSetor(Long id)
    {
        Setor result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getSetorCount()
    {
        return lista.size();
    }

    public Setor findSetorBySigla(String sigla)
    {
        Setor result = null;
        if(sigla != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(sigla.equalsIgnoreCase(lista.get(i).getSigla()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }
}