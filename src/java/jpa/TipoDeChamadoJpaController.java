package jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import model.Chamado;
import model.TipoDeChamado;
import jpa.exceptions.IllegalOrphanException;
import jpa.exceptions.NonexistentEntityException;
import jpa.exceptions.RollbackFailureException;

/**
 *
 * @author Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
 */
public class TipoDeChamadoJpaController implements Serializable
{
    private static final long serialVersionUID = 692305792669767688L;

    private static final List<TipoDeChamado> lista = new ArrayList<>();
    
    static
    {
        TipoDeChamado tdc;
        
        tdc = new TipoDeChamado();
        tdc.setId(new Long("0"));
        tdc.setNome("Formatacao");
        lista.add(tdc);
        
        tdc = new TipoDeChamado();
        tdc.setId(new Long("1"));
        tdc.setNome("Troca de toner");
        lista.add(tdc);
        
        tdc = new TipoDeChamado();
        tdc.setId(new Long("2"));
        tdc.setNome("Outros");
        lista.add(tdc);
    }

    public TipoDeChamadoJpaController(UserTransaction utx, EntityManagerFactory emf)
    {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager()
    {
        return emf.createEntityManager();
    }

    public void create(TipoDeChamado tipoDeChamado) throws RollbackFailureException, Exception
    {
        TipoDeChamado tdc = findTipoDeChamado(tipoDeChamado.getId());
        if(tdc == null)
        {
            tipoDeChamado.setId(new Long(lista.size()));
            lista.add(tipoDeChamado);
        }
        else
        {
            throw new EntityExistsException("Tipo de Chamando já existe");
        }
    }

    public void edit(TipoDeChamado tipoDeChamado) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        TipoDeChamado tdc = findTipoDeChamado(tipoDeChamado.getId());
        if(tdc != null)
        {
            tdc.setNome(tipoDeChamado.getNome());
        }
        else
        {
            throw new NonexistentEntityException("Este Tipo de Chamando não existe");
        }
    }

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception
    {
        TipoDeChamado tdc = findTipoDeChamado(id);
        if(tdc != null)
        {
            ChamadoJpaController cjc = new ChamadoJpaController(utx, emf);
            List<Chamado> list = cjc.findChamadosByTipo(tdc);
            if(list.isEmpty() == false)
            {
                List<String> strings = new ArrayList<>();
                for(Chamado chamado: list)
                {
                    strings.add("Este Chamado ficará orfão " + chamado);
                }
                throw new IllegalOrphanException(strings);
            }
            lista.remove(tdc);
        }
        else
        {
            throw new NonexistentEntityException("Este Tipo de Chamando não existe");
        }
    }

    public List<TipoDeChamado> findTipoDeChamadoEntities()
    {
        return findTipoDeChamadoEntities(true, -1, -1);
    }

    public List<TipoDeChamado> findTipoDeChamadoEntities(int maxResults, int firstResult)
    {
        return findTipoDeChamadoEntities(false, maxResults, firstResult);
    }

    private List<TipoDeChamado> findTipoDeChamadoEntities(boolean all, int maxResults, int firstResult)
    {
        ArrayList<TipoDeChamado> result = new ArrayList<>();
        if(all == true)
        {
            firstResult = 0;
            maxResults = lista.size();
        }
        for(int i = firstResult; i < firstResult + maxResults; i++)
        {
            result.add(lista.get(i));
        }
        return result;
    }

    public TipoDeChamado findTipoDeChamado(Long id)
    {
        TipoDeChamado result = null;
        if(id != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(id.equals(lista.get(i).getId()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }

    public int getTipoDeChamadoCount()
    {
        return lista.size();
    }

    public TipoDeChamado findTipoDeChamadoByNome(String nomeantigo)
    {
        TipoDeChamado result = null;
        if(nomeantigo != null)
        {
            for(int i = 0; result == null && i < lista.size(); i++)
            {
                if(nomeantigo.equals(lista.get(i).getNome()))
                {
                    result = lista.get(i);
                }
            }
        }
        return result;
    }
}