<%-- 
    Document   : telaprincipal
    Created on : 09/11/2014, 18:55:18
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.Tecnico"%>
<%@page import="model.Administrador"%>
<%@page import="model.Usuario"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            Usuario u = (Usuario) session.getAttribute("usuario");
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <div class="band">
            <div class="container">
                <div class="sixteen columns">
                    <h2>Bem vindo, <c:out value="${usuario.nome}" /></h2>
                </div>
            </div>
        </div>
        <div class="band navigation">
            <nav class="container primary">
                <div class="sixteen columns">
                    <ul>
                        <li><a href="#">Sistema</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/Sair">Efetuar logoff</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atualizardados.jsp">Atualizar meus dados</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Chamados</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/verchamados.jsp">Ver chamados</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrarchamado.jsp">Cadastrar chamado</a></li>
                            </ul>
                        </li>
                        <c:if test="${usuario.tipo == 'tecnico'}">
                        <li><a href="#">Tipos de Chamado</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrartipochamado.jsp">Cadastrar Tipo de Chamado</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atualizartipochamado.jsp">Atualizar Tipo de Chamado</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/vertiposchamados.jsp">Ver Tipos de Chamados</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/excluirtipochamado.jsp">Excluir Tipo de Chamado</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Tipos de Equipamento</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atualizartipoequipamento.jsp">Atualizar Tipo de Equipamento</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrartipoequipamento.jsp">Cadastrar Tipo de Equipamento</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/vertiposdeequipamentos.jsp">Ver Tipos de Equipamento</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/excluirtiposdeequipamentos.jsp">Excluir Tipos de Equipamento</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Equipamentos</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atualizarequipamento.jsp">Atualizar Equipamento</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrarequipamento.jsp">Cadastrar Equipamento</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/verequipamentos.jsp">Ver Equipamentos</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/excluirequipamentos.jsp">Excluir Equipamento</a></li>
                            </ul>
                        </li>
                        </c:if>
                        <c:if test="${usuario.tipo == 'administrador'}">
                        <li><a href="#">Usuários</a>
                            <ul>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atualizarusuario.jsp">Atualizar Usuário</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrarusuario.jsp">Cadastrar Usuário</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/autorizarusuarios.jsp">Autorizar Usuários</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/verusuarios.jsp">Ver Usuários</a></li>
                                <li><a href="<c:out value="${pageContext.servletContext.contextPath}" />/excluirusuario.jsp">Excluir Usuário</a></li>
                            </ul>
                        </li>
                        </c:if>
                    </ul>
                </div>
            </nav>
        </div>
    </body>
</html>