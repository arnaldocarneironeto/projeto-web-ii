<%-- 
    Document   : excliurequipamentos
    Created on : 09/11/2014, 19:33:39
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.EquipamentoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Equipamento"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            EquipamentoDao ed = daoFactory.createEquipamentoDao();
            List<Equipamento> es = ed.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/ExcluirEquipamento" method="post">
            <fieldset>
                <label for="tombamento">Tombamento:</label>
                <select name="tombamento" id="tombamento">
                    <%for(Equipamento e: es){%>
                    <option><%=e.getTombamento()%></option>
                    <%}%>
                </select>
                <input type="submit" value="Confirmar exclusão" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>