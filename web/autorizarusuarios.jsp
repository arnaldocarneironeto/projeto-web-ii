<%-- 
    Document   : atualizarusuario
    Created on : 09/11/2014, 19:27:35
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.UsuarioDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            UsuarioDao ud = daoFactory.createUsuarioDao();
            List<Usuario> list = ud.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/Autorizar" method="POST">
            <table>
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>Sala</th>
                        <th>E-mail</th>
                        <th>Setor</th>
                        <th>Autorizar?</th>
                    </tr>
                </thead>
                <tbody>
                    <%for(Usuario u : list){
                        if(u.isFoiAprovado() == false){%>
                    <tr>
                        <td><%=u.getLogin()%></td>
                        <td><%=u.getNome()%></td>
                        <td><%=u.getTelefone()%></td>
                        <td><%=u.getSala()%></td>
                        <td><%=u.getEmail()%></td>
                        <td><%=u.getSetor().getSigla()%></td>
                        <td><input type="checkbox" name="ch<%=u.getLogin()%>" /></td>
                    </tr>
                    <%}}%>
                </tbody>
            </table>
            <input type="submit" value="Autorizar marcados" />
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>