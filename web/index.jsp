<%-- 
    Document   : index
    Created on : 09/12/2014, 14:04:57
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <meta http-equiv="refresh" content="0; url=Main" />
    </head>
    <body>
        <h1>Carregando...</h1>
    </body>
</html>
