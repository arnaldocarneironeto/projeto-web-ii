<%-- 
    Document   : atualequip
    Created on : 09/11/2014, 19:33:39
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="java.util.List"%>
<%@page import="model.Equipamento"%>
<%@page import="model.TipoDeEquipamento"%>
<%@page import="model.dao.TipoDeEquipamentoDao"%>
<%@page import="model.dao.EquipamentoDao"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="control.FactoryControl"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            String tombamento = request.getParameter("tombamento");
            DaoFactory daoFactory = FactoryControl.getDaoFactory();

            EquipamentoDao ed = daoFactory.createEquipamentoDao();
            Equipamento e = ed.find(tombamento);

            TipoDeEquipamentoDao tded = daoFactory.createTipoDeEquipamentoDao();
            List<TipoDeEquipamento> tdes = tded.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <% if(e != null) {%>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/AtualizarEquipamento" method="post">
            <fieldset>
                <fieldset>
                    <label for="tipo">Tipo:</label>
                    <select name="tipo" id="tipo">
                        <%for(TipoDeEquipamento tde: tdes){%>
                        <option <%
                                    if(tde.equals(e.getTipo()))
                                    {
                                        out.print("selected=\"selected\"");
                                    }
                                %>><%=tde.getNome()%></option>
                        <%}%>
                    </select>
                    <label for="tombamento">Tombamento:</label>
                    <input type="text" name="tombamento" id="tombamento" value="<%=e.getTombamento()%>" readonly="readonly" />
                    <label for="descricao">Descrição:</label>
                    <input type="text" name="descricao" id="descricao" value="<%=e.getDescricao()%>" />
                    <label for="ativo">Ativo:</label>
                    <input type="radio" name="ativo" id="ativo" value="ativo"
                           <%
                            if(e.isAtivo() == true)
                            {
                                out.print("checked=\"checked\"");
                            }
                           %>
                           >
                    <label for="inativo">Inativo:</label>
                    <input type="radio" name="ativo" id="inativo" value="inativo"
                           <%
                            if(e.isAtivo() == false)
                            {
                                out.print("checked=\"checked\"");
                            }
                           %>
                           >
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
         <%}%>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>