<%-- 
    Document   : atualuser
    Created on : 17/11/2014, 01:50:38
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.Administrador"%>
<%@page import="model.Tecnico"%>
<%@page import="model.dao.UsuarioDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Usuario"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            String login = request.getParameter("login");
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            
            UsuarioDao ud = daoFactory.createUsuarioDao();
            Usuario u = ud.find(login);
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <% if(u != null) {%>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/AtualizarUsuario" method="post">
            <fieldset>
                <fieldset>
                    <label for="tipo">Tipo:</label>
                    <select name="tipo" id="tipo">
                        <option <%if(u.getTipo().equals("usuario")){%>selected="selected"<%}%>>Padrão</option>
                        <option <%if(u.getTipo().equals("tecnico")){%>selected="selected"<%}%>>Técnico</option>
                        <option <%if(u.getTipo().equals("administrador")){%>selected="selected"<%}%>>Adminstrador</option>
                    </select>
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" id="nome" value="<%=u.getNome()%>" />
                    <label for="email">E-mail:</label>
                    <input type="text" name="email" id="email" value="<%=u.getEmail()%>" />
                    <label for="telefone">Telefone:</label>
                    <input type="text" name="telefone" id="telefone" value="<%=u.getTelefone()%>" />
                    <label for="setor">Setor:</label>
                    <input type="text" name="setor" id="setor" value="<%=u.getSetor().getSigla()%>" />
                    <label for="sala">Sala:</label>
                    <input type="text" name="sala" id="sala" value="<%=u.getSala()%>" />
                    <label for="ativo">Ativo:</label>
                    <input type="radio" name="ativo" id="ativo" value="ativo"
                           <%
                            if(u.isAtivo() == true)
                            {
                                out.print("checked=\"checked\"");
                            }
                           %>
                           >
                    <label for="inativo">Inativo:</label>
                    <input type="radio" name="ativo" id="inativo" value="inativo"
                           <%
                            if(u.isAtivo() == false)
                            {
                                out.print("checked=\"checked\"");
                            }
                           %>
                           >
                </fieldset>
                <fieldset>
                    <label for="senha">Senha do Adminstrador:</label>
                    <input type="password" name="senha" id="senha" />
                    <label for="login">Login do Usuário:</label>
                    <input type="text" name="login" id="login" value="<%=u.getLogin()%>" readonly="readonly" />
                    <label for="novasenha">Nova Senha do Usuário:</label>
                    <input type="password" name="novasenha" id="novasenha" />
                    <label for="confirmasenha">Confirma Nova Senha do Usuário:</label>
                    <input type="password" name="confirmasenha" id="confirmasenha" />
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
         <%}%>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>