<%-- 
    Document   : excluirtipochamado
    Created on : 09/11/2014, 19:26:25
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.TipoDeChamadoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.TipoDeChamado"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            TipoDeChamadoDao tdcd = daoFactory.createTipoDeChamadoDao();
            List<TipoDeChamado> tdcs = tdcd.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Tipo de Chamado</th>
                </tr>
            </thead>
            <tbody>
                <%for(TipoDeChamado tdc: tdcs){%>
                <tr>
                    <td><%=tdc.getId()%></td>
                    <td><%=tdc.getNome()%></td>
                </tr>
                <%}%>
            </tbody>
        </table>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>