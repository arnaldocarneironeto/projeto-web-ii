<%-- 
    Document   : erroToken2
    Created on : 08/11/2014, 23:24:57
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Token inválido, senhas não coincidem ou senha não obedece um padrão mínimo de segurança.</p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>