<%-- 
    Document   : excluirtiposdeequipamentos
    Created on : 09/11/2014, 19:26:25
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.TipoDeEquipamentoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.TipoDeEquipamento"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            TipoDeEquipamentoDao tded = daoFactory.createTipoDeEquipamentoDao();
            List<TipoDeEquipamento> tdes = tded.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Tipo de Equipamento</th>
                </tr>
            </thead>
            <tbody>
                <%for(TipoDeEquipamento tde: tdes){%>
                <tr>
                    <td><%=tde.getId()%></td>
                    <td><%=tde.getNome()%></td>
                </tr>
                <%}%>
            </tbody>
        </table>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>