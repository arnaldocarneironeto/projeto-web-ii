<%-- 
    Document   : atualizartipochamado
    Created on : 09/11/2014, 19:26:25
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="java.util.List"%>
<%@page import="model.TipoDeChamado"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.dao.TipoDeChamadoDao"%>
<%@page import="control.FactoryControl"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            TipoDeChamadoDao tdcd = daoFactory.createTipoDeChamadoDao();
            List<TipoDeChamado> tdcs = tdcd.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/AlterarTipoDeChamado" method="post">
            <fieldset>
                <fieldset>
                    <label for="nomeantigo">Nome antigo do tipo de chamado:</label>
                    <select name="nomeantigo" id="nomeantigo">
                        <%for(TipoDeChamado tdc: tdcs){%>
                        <option><%=tdc.getNome()%></option>
                        <%}%>
                    </select>
                    <label for="nome">Novo nome do tipo de chamado:</label>
                    <input type="text" name="nome" id="nome" />
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>