<%-- 
    Document   : index
    Created on : 09/11/2014, 15:47:16
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>
<%@taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
    </head>
    <body>
        <c:import url="header.jsp" />
        <div class="band navigation">
            <nav class="container primary">
                <div class="sixteen columns">
                    <p>&nbsp;</p>
                </div>
            </nav>
        </div>
        <div class="band">
            <div class="container">
                <div class="sixteen columns">
                    <form action="<c:out value="${pageContext.servletContext.contextPath}" />/Login" method="post">
                        <fieldset>
                            <label for="login">Login:</label>
                            <input type="text" name="login" id="login" />
                            <label for="senha">Senha:</label>
                            <input type="password" name="senha" id="senha" />
                            <input type="submit" value="Realizar login"/>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="band footer">
            <div class="container">
                <div class="eight columns">
                    <fieldset>
                        <a href="<c:out value="${pageContext.servletContext.contextPath}" />/recuperarsenha.jsp" class="button">Esqueceu a senha?</a>
                    </fieldset>
                </div>
                <div class="eight columns">
                    <fieldset>
                        <a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrar-se.jsp" class="button">Ainda não é registrado?</a>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="band bottom">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns">
                        <fieldset>
                            <a href="<c:out value="${pageContext.servletContext.contextPath}" />/sobre.jsp" class="button">Sobre</a>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>