<%-- 
    Document   : sobre
    Created on : 09/11/2014, 15:52:00
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Criado e mantido por</p>
        <ul>
            <li>Arnaldo Carneiro</li>
            <li>Diogo Dias</li>
            <li>Renato Rodrigues</li>
        </ul>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />/contato.jsp" class="button">Entre em contato conosco</a></p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>
