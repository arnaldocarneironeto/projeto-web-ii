<%-- 
    Document   : cadastrar-se
    Created on : 09/11/2014, 19:29:03
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
    </head>
    <body>
        <h1><c:out value="${initParam.appName}" /></h1>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/CadastrarSe" method="post">
            <fieldset>
                <fieldset>
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" id="nome" />
                    <label for="telefone">Telefone:</label>
                    <input type="text" name="telefone" id="telefone" />
                    <label for="setor">Setor:</label>
                    <input type="text" name="setor" id="setor" />
                    <label for="sala">Sala:</label>
                    <input type="text" name="sala" id="sala" />
                    <label for="email">E-mail:</label>
                    <input type="text" name="email" id="email" />
                </fieldset>
                <fieldset>
                    <label for="login">Login:</label>
                    <input type="text" name="login" id="login" />
                    <label for="senha">Senha:</label>
                    <input type="password" name="senha" id="senha" />
                    <label for="confirmasenha">Confirmar Senha:</label>
                    <input type="password" name="confirmasenha" id="confirmasenha" />
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />">Voltar</a></p>
    </body>
</html>