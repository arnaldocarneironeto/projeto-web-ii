<%-- 
    Document   : excluirtiposdeequipamentos
    Created on : 09/11/2014, 19:26:25
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.TipoDeEquipamentoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.TipoDeEquipamento"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            TipoDeEquipamentoDao tded = daoFactory.createTipoDeEquipamentoDao();
            List<TipoDeEquipamento> tdes = tded.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/ExcluirTipoDeEquipamento" method="post">
            <fieldset>
                <fieldset>
                    <label for="nomeantigo">Nome do tipo de equipamento:</label>
                    <select name="nomeantigo" id="nomeantigo">
                        <%for(TipoDeEquipamento tde: tdes){%>
                        <option><%=tde.getNome()%></option>
                        <%}%>
                    </select>
                </fieldset>
                <input type="submit" value="Confirmar exclusão" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>