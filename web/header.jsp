        <%@taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
        <div class="band header">
            <header class="container main">
                <div class="four columns">
                    <h1 class="logo"><a href="<c:out value="${pageContext.servletContext.contextPath}" />">Logo</a></h1>
                </div>
                <div class="twelve columns">
                    <h1><c:out value="${initParam.appName}" /></h1>
                </div>
            </header>
        </div>
