<%-- 
    Document   : autorizarusuarios
    Created on : 09/11/2014, 19:27:35
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.UsuarioDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            UsuarioDao ud = daoFactory.createUsuarioDao();
            List<Usuario> us = ud.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/atualuser.jsp" method="post">
            <fieldset>
                <label for="login">Login:</label>
                <select name="login" id="login">
                    <%for(Usuario u: us){%>
                    <option><%=u.getLogin()%></option>
                    <%}%>
                </select>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>