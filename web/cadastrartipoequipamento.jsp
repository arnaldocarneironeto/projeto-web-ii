<%-- 
    Document   : cadastrartipoequipamento
    Created on : 09/11/2014, 19:33:39
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/CadastrarTipoDeEquipamento" method="post">
            <fieldset>
                <fieldset>
                    <label for="nome">Novo tipo de equipamento:</label>
                    <input type="text" name="nome" id="nome" />
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>