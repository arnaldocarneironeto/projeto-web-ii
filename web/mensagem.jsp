<%-- 
    Document   : erroToken1
    Created on : 08/11/2014, 23:24:57
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
    </head>
    <body>
        <c:import url="header.jsp" />
        <%
            String mensagem = (String) session.getAttribute("mensagem");
            if(mensagem == null)
            {
                mensagem = "Ocorreu algum erro inesperado.";
            }
        %>
        <p><%=mensagem%></p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>