<%-- 
    Document   : recuperarsenha
    Created on : 08/11/2014, 21:56:34
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Digite seu e-mail cadastrado no campo abaixo para que possamos enviar instruções de recuperação de senha.</p>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/RecuperarSenha" method="post">
            <fieldset>
                <label for="email">E-mail:</label>
                <input type="text" name="email" id="email" />
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>
