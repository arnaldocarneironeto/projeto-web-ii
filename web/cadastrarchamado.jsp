<%-- 
    Document   : cadastrarchamado
    Created on : 09/11/2014, 19:28:09
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.TipoDeChamadoDao"%>
<%@page import="model.dao.EquipamentoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.TipoDeChamado"%>
<%@page import="java.util.List"%>
<%@page import="model.Equipamento"%>
<%@page import="model.Usuario"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            Usuario u = (Usuario) session.getAttribute("usuario");
            
            EquipamentoDao ed = daoFactory.createEquipamentoDao();
            List<Equipamento> es = ed.list();
            
            TipoDeChamadoDao tdcd = daoFactory.createTipoDeChamadoDao();
            List<TipoDeChamado> tdcs = tdcd.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <h1>Cadastrar Chamado</h1>
        <%if(u != null){%>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/CadastrarChamado" method="post">
            <fieldset>
                <label for="tipo">Tipo de Chamado:</label>
                <select name="tipo" id="tipo">
                    <%for(TipoDeChamado tdc: tdcs){%>
                    <option value="<%=tdc.getId()%>"><%=tdc.getNome()%></option>
                    <%}%>
                </select>
                <label for="nome">Nome:</label>
                <input type="text" name="nome" id="nome" value="<%=u.getLogin()%>" readonly="readonly" />
                <label for="tombamento">Tombamento:</label>
                <select name="tombamento" id="tombamento">
                    <option></option>
                    <%for(Equipamento e: es){if(e.isAtivo() == true){%>
                    <option value="<%=e.getTombamento()%>"><%=e.getTombamento() + " - " + e.getDescricao()%></option>
                    <%}}%>
                </select>
            </fieldset>
            <fieldset>
                <label for="descricao">Descrição do Problema</label><br>
                <textarea cols="80" rows="5" name="descricao" id="descricao"></textarea>
            </fieldset>
            <input type="submit" value="Enviar" />
        </form>
        <%}%>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>