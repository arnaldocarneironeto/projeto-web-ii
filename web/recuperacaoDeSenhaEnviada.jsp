<%-- 
    Document   : recuperacaoDeSenhaEnviada
    Created on : 08/11/2014, 20:42:27
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Caso exista um usuário com este e-mail cadastrado em nosso banco de dados, um e-mail com as informações de recuperação de senha para o e-mail informado.</p>
        <p>E-mail informado: <%=request.getParameter("email") %></p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>