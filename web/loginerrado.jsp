<%-- 
    Document   : loginerrado
    Created on : 09/11/2014, 18:53:27
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>O nome de usuário digitado não existe em nosso cadastro ou a senha é inválida.</p>
        <fieldset>
            <a href="<c:out value="${pageContext.servletContext.contextPath}" />/recuperarsenha.jsp" class="button">Esqueceu a senha?</a>
        </fieldset>
        <fieldset>
            <a href="<c:out value="${pageContext.servletContext.contextPath}" />/cadastrar-se.jsp" class="button">Ainda não é registrado?</a>
        </fieldset>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>
