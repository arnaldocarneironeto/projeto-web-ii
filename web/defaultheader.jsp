        <%@taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="stylesheets/base.css" />
        <link type="text/css" rel="stylesheet" href="stylesheets/skeleton.css" />
        <link type="text/css" rel="stylesheet" href="stylesheets/layout.css" />
        <link type="text/css" rel="stylesheet" href="stylesheets/tables.css" />
        <link rel="shortcut icon" href="icons/favicon.ico">
        <link rel="apple-touch-icon" href="icons/icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="icons/icon72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="icons/icon114.png">
        <title><c:out value="${initParam.appName}" /></title>
        <script src="javascripts/jquery-1.7.1.min.js"></script>
        <script src="javascripts/tabs.js"></script>
        <script src="javascripts/menu.js"></script>