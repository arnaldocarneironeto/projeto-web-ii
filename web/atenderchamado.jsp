<%-- 
    Document   : atualizarequipamento
    Created on : 09/11/2014, 19:24:52
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="java.util.List"%>
<%@page import="model.Estado"%>
<%@page import="model.Acao"%>
<%@page import="model.Tecnico"%>
<%@page import="model.ChamadoDeEquipamento"%>
<%@page import="model.Equipamento"%>
<%@page import="model.Chamado"%>
<%@page import="model.Usuario"%>
<%@page import="model.dao.UsuarioDao"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.dao.ChamadoDao"%>
<%@page import="control.FactoryControl"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            Usuario u = (Usuario) session.getAttribute("usuario");
            DaoFactory daoFactory = FactoryControl.getDaoFactory();

            ChamadoDao cd = daoFactory.createChamadoDao();
            Chamado c = cd.find(new Long(request.getParameter("id")));

            Equipamento e = null;
            if(c.temEquipamentoAssociado())
            {
                e = ((ChamadoDeEquipamento) c).getEquipamento();
            }

            UsuarioDao ud = daoFactory.createUsuarioDao();
            List<Usuario> us = ud.list();
            
            List<Acao> as = c.getListaDeAcoes();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/AtenderChamado" method="post">
            <fieldset>
                <input type="hidden" name="chamadoid" value="<%=c.getId()%>" />
                <label for="nome">Nome</label>
                <input type="text" name="nome" id="nome" value="<%=u.getNome()%>" readonly="readonly" />
                <label for="tombamento">Equipamento:</label>
                <input type="text" name="tombamento" id="tombamento" <%if(e != null){%> value="<%=e.getTombamento()%>" <%}%>readonly="readonly" />
                <label for="sala">Sala:</label>
                <input type="text" name="sala" id="sala" value="<%=u.getSala()%>" readonly="readonly" />
                <label for="descricao">Descrição do problema:</label>
                <textarea cols="80" rows="5" name="descricao" id="descricao" readonly="readonly"><%=c.getDescricaoDoProblema()%></textarea>
                <%if(as != null){%>
                <label for="acoes">O que foi feito anteriormente:</label>
                <textarea cols="80" rows="5" name="acoes" id="acoes" readonly="readonly"><%for(Acao a: as){out.println(a.getTecnico().getNome() + " fez: " + a.getDescricaoDoQueFoiFeito());out.println("Falta fazer: " + a.getDescricaoDoQueDeveSerFeito());}%></textarea>
                <%}%>
                <%if(c.getEstado().equals(Estado.ABERTO)){%>
                <label for="foifeito">O que eu fiz:</label>
                <textarea cols="80" rows="5" name="foifeito" id="foifeito"></textarea>
                <label for="faltafazer">O que deve ser feito por outro técnico:</label>
                <textarea cols="80" rows="5" name="faltafazer" id="faltafazer"></textarea>
                <label for="logintecnico">O outro técnico:</label>
                <select name="logintecnico" id="logintecnico">
                    <option></option>
                    <%for(Usuario usuario: us){if(usuario.getTipo().equals("tecnico")){%><option><%=usuario.getLogin()%></option><%}}%>
                </select>
                <label for="fechar">Fechar chamado?</label>
                <input type="checkbox" name="fechar" id="fechar" />
                <input type="submit" value="Encaminhar" />
                <%}%>
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>