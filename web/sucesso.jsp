<%-- 
    Document   : sucesso
    Created on : 08/11/2014, 23:24:57
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Sua senha foi alterada com sucesso.</p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>