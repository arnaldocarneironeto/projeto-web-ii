<%-- 
    Document   : cadastrarequipamento
    Created on : 09/11/2014, 19:33:39
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.TipoDeEquipamentoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.TipoDeEquipamento"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            TipoDeEquipamentoDao tded = daoFactory.createTipoDeEquipamentoDao();
            List<TipoDeEquipamento> tdes = tded.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/CadastrarEquipamento" method="post">
            <fieldset>
                <fieldset>
                    <label for="tipo">Tipo:</label>
                    <select name="tipo" id="tipo">
                        <%for(TipoDeEquipamento tde: tdes){%>
                        <option><%=tde.getNome()%></option>
                        <%}%>
                    </select>
                    <label for="tombamento">Tombamento:</label>
                    <input type="text" name="tombamento" id="tombamento" />
                    <label for="descricao">Descrição:</label>
                    <input type="text" name="descricao" id="descricao" />
                    <label for="ativo">Ativo:</label>
                    <input type="radio" name="ativo" id="ativo" value="ativo" checked="checked">
                    <label for="inativo">Inativo:</label>
                    <input type="radio" name="ativo" id="inativo" value="inaativo" disabled="disabled">
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>