<%-- 
    Document   : verusuarios
    Created on : 09/11/2014, 19:32:41
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.UsuarioDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Usuario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            UsuarioDao ud = daoFactory.createUsuarioDao();
            List<Usuario> list = ud.list();
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <table>
            <thead>
                <tr>
                    <th>Login</th>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Sala</th>
                    <th>E-mail</th>
                    <th>Setor</th>
                    <th>Ativo?</th>
                </tr>
            </thead>
            <tbody>
                <%for(Usuario u : list){%>
                <tr>
                    <td><%=u.getLogin()%></td>
                    <td><%=u.getNome()%></td>
                    <td><%=u.getTelefone()%></td>
                    <td><%=u.getSala()%></td>
                    <td><%=u.getEmail()%></td>
                    <td><%=u.getSetor().getSigla()%></td>
                    <td><input type="checkbox" name="ativo" <%
                        if(u.isAtivo())
                        {
                            out.print("checked=\"checked\"");
                        }
                    %> disabled="disabled" /></td>
                </tr>
                <%}%>
            </tbody>
        </table>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>