<%-- 
    Document   : dadosinvalidos
    Created on : 09/11/2014, 19:20:14
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
    </head>
    <body>
        <c:import url="header.jsp" />
        <p>Dados inválidos</p>
        <p><%=request.getParameter("mensagem")%></p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>
