<%-- 
    Document   : mensagemEnviada
    Created on : 10/11/2014, 11:26:52
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
            Boolean wasTheMessageSent = (Boolean) request.getAttribute("wasTheMessageSent");
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <p><%=wasTheMessageSent?"Sua mensagem foi enviada com sucesso.":"Houve um erro no envio da sua mensagem."%></p>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>