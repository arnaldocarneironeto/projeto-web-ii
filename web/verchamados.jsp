<%-- 
    Document   : verchamados
    Created on : 09/11/2014, 19:31:44
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.dao.ChamadoDao"%>
<%@page import="control.FactoryControl"%>
<%@page import="model.dao.DaoFactory"%>
<%@page import="model.Equipamento"%>
<%@page import="model.ChamadoDeEquipamento"%>
<%@page import="model.Acao"%>
<%@page import="java.util.List"%>
<%@page import="model.Chamado"%>
<%@page import="model.Tecnico"%>
<%@page import="model.Usuario"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            DaoFactory daoFactory = FactoryControl.getDaoFactory();
            Usuario u = (Usuario) session.getAttribute("usuario");
            ChamadoDao cd = daoFactory.createChamadoDao();
            List<Chamado> cs;
            if(u.getTipo().equals("tecnico"))
            {
                // mostrar todos os chamados
                cs = cd.list();
                
            }
            else
            {
                // mostrar apenas os chamados deste usuário
                cs = cd.findByUsuario(u);
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <table>
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Estado</th>
                    <th>Usuário</th>
                    <th>E-mail</th>
                    <th>Técnico Responsável</th>
                    <th>Descrição do Problema</th>
                    <th>Tipo</th>
                    <th>Última Ação</th>
                    <th>Equipamento</th>
                    <%if(u.getTipo().equals("tecnico")){%>
                    <th>Atender</th>
                    <%}%>
                </tr>
            </thead>
            <tbody>
                <%for(Chamado c: cs){%>
                <tr>
                    <td><%=c.getId()%></td>
                    <td><%=c.getEstado().name()%></td>
                    <td><%=c.getUsuario().getLogin() + " " + c.getUsuario().getNome()%></td>
                    <%if(c.getTecnicoResponsavel() != null){%>
                    <td><%=c.getTecnicoResponsavel().getEmail()%></td>
                    <td><%=c.getTecnicoResponsavel().getLogin() + " " + c.getTecnicoResponsavel().getNome()%></td>
                    <%}else{%>
                    <td></td>
                    <td></td>
                    <%}%>
                    <td><%=c.getDescricaoDoProblema()%></td>
                    <td><%=c.getTipo().getNome()%></td>
                    <td>
                        <%
                            List<Acao> l = c.getListaDeAcoes();
                            if(l != null && l.isEmpty() == false)
                            {
                                out.print(l.get(l.size() - 1).getDescricaoDoQueFoiFeito());
                            }
                        %>
                    </td>
                    <td>
                        <%
                            if(c.temEquipamentoAssociado())
                            {
                                Equipamento e = ((ChamadoDeEquipamento) c).getEquipamento();
                                if(e != null)
                                {
                                    out.print(e.getTombamento() + " " + e.getTipo().getNome());
                                }
                            }
                        %>
                    </td>
                    <%if(u.getTipo().equals("tecnico")){%>
                    <td><a href="<c:out value="${pageContext.servletContext.contextPath}" />/atenderchamado.jsp?id=<%=c.getId()%>" class="button">&#9654;</a></td>
                    <%}%>
                </tr>
                <%}%>
            </tbody>
        </table>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>