<%-- 
    Document   : contato
    Created on : 09/11/2014, 15:57:05
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            if(session != null)
            {
                session.invalidate();
                session = null;
            }
        %>
    </head>
    <body>
        <h1>Entre em contato conosco - <c:out value="${initParam.appName}" /></h1>
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/EnviarMensagem" method="post">
            <fieldset>
                <label for="nome">Nome</label>
                <input type="text" name="nome" id="nome" />
                <label for="emailparacontato">E-mail para contato:</label>
                <input type="text" name="emailparacontato" id="emailparacontato" />
                <label for="assunto">Assunto:</label>
                <input type="text" name="assunto" id="assunto" />
                <label for="mensagem">Mensagem:</label>
                <textarea cols="80" rows="5" name="mensagem" id="mensagem"></textarea>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />">Voltar</a></p>
    </body>
</html>