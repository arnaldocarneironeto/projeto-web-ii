<%-- 
    Document   : atualizardados
    Created on : 09/11/2014, 19:16:49
    Author     : Arnaldo Carneiro <acsn@a.recife.ifpe.edu.br>
--%>

<%@page import="model.Setor"%>
<%@page import="java.lang.Boolean"%>
<%@page import="model.Usuario"%>
<%@ taglib prefix="c"uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <c:import url="defaultheader.jsp" />
        <%
            Usuario usuario;
            Setor setor;
            usuario = (Usuario) session.getAttribute("usuario");
            if(usuario == null)
            {
                usuario = new Usuario();
                usuario.setNome("Você não está logado no sistema");
                usuario.setLogin("---");
                setor = new Setor();
                setor.setSigla("---");
                usuario.setSetor(setor);
            }
        %>
    </head>
    <body>
        <c:import url="header.jsp" />
        <form action="<c:out value="${pageContext.servletContext.contextPath}" />/AtualizarDados" method="post">
            <fieldset>
                <fieldset>
                    <label for="nome">Nome:</label>
                    <input type="text" name="nome" id="nome" value="<%=usuario.getNome()%>"/>
                    <label for="telefone">Telefone:</label>
                    <input type="text" name="telefone" id="telefone" value="<%=usuario.getTelefone()%>" />
                    <label for="setor">Setor:</label>
                    <input type="text" name="setor" id="setor" value="<%=usuario.getSetor().getSigla()%>" />
                    <label for="sala">Sala:</label>
                    <input type="text" name="sala" id="sala" value="<%=usuario.getSala()%>" />
                    <label for="email">E-mail:</label>
                    <input type="text" name="email" id="email" value="<%=usuario.getEmail()%>" />
                </fieldset>
                <fieldset>
                    <label for="login">Login:</label>
                    <input type="text" name="login" id="login" value="<%=usuario.getLogin()%>" readonly="readonly" />
                    <label for="senha">Senha:</label>
                    <input type="password" name="senha" id="senha" />
                    <label for="novasenha">Nova Senha:</label>
                    <input type="password" name="novasenha" id="novasenha" />
                    <label for="confirmasenha">Confirmar Nova Senha:</label>
                    <input type="password" name="confirmasenha" id="confirmasenha" />
                </fieldset>
                <input type="submit" value="Enviar" />
            </fieldset>
        </form>
        <p><a href="<c:out value="${pageContext.servletContext.contextPath}" />" class="button">Voltar</a></p>
    </body>
</html>